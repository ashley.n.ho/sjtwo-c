//
//  CustomMapViewController.swift
//  TeamX_App
//
//  Created by Priyanka Shah on 5/1/24.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate: AnyObject {
    func didChooseLocation(latitude: Double, longitude: Double)
}


class CustomMapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    weak var delegate: MapViewControllerDelegate?
    
    var lastSelectedLatitude: Double?
    var lastSelectedLongitude: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self

        print("Map View loaded!")
        // Add a tap gesture recognizer to the map view
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(_:)))
        mapView.addGestureRecognizer(tapRecognizer)
        
        // Define the center of the region your map should display
        // 37.33919199814887, -121.88103575173972
        let center = CLLocationCoordinate2D(latitude: 37.33919199814887, longitude: -121.88103575173972) // SJSU parking lot
        
//        let center = CLLocationCoordinate2D(latitude: 37.3363960, longitude: -121.8808527) // SJSU student union
//        let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002) // The zoom level
        
        
        //let center = CLLocationCoordinate2D(latitude: 37.3373015, longitude: -121.8797205) // Wells fargo
        let span = MKCoordinateSpan(latitudeDelta: 0.0003, longitudeDelta: 0.0003) // The zoom level
        
        
        // Set the region of the map
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: true)
    }

    @objc func handleMapTap(_ gestureReconizer: UITapGestureRecognizer) {
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)

        // Remove all existing annotations
        mapView.removeAnnotations(mapView.annotations)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        
        lastSelectedLatitude = coordinate.latitude
        lastSelectedLongitude = coordinate.longitude
        
        print("Latitude: \(coordinate.latitude), Longitude: \(coordinate.longitude)")
        
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        print("Sending GPS destination location to Bridge Controller!")
        
        if let currentlatitude = lastSelectedLatitude, let currentlongitude = lastSelectedLongitude {
            delegate?.didChooseLocation(latitude: currentlatitude, longitude: currentlongitude)
            print("Coordinates sent: Latitude: \(currentlatitude), Longitude: \(currentlongitude)")
        } else {
            print("No location selected.")
        }
    }
    
}
