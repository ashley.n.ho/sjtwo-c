//
//  ViewController.swift
//  TeamX_App
//
//  Created by Priyanka Shah on 4/13/24.
//

import UIKit
import CoreBluetooth
import MapKit


class ViewController: UIViewController {

    
    @IBOutlet weak var displayContent: UITableView!
    @IBOutlet weak var messageLabel: UILabel!

    
    var centralManager: CBCentralManager!
    var discoveredDevices: [CBPeripheral] = []
    var selectedPeripheral: CBPeripheral?
    var writeCharacteristic: CBCharacteristic?
    var readCharacteristic: CBCharacteristic?
    var globalCoordinateString: String = "0,0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        centralManager = CBCentralManager(delegate: self, queue: nil)
        print("Will add details here")
        
        displayContent.delegate = self
        // Set up the table view data source
        displayContent.dataSource = self
        // If you are not using a custom UITableViewCell, register a default one
        displayContent.register(UITableViewCell.self, forCellReuseIdentifier: "DeviceCell")
        print("Table view set up done")
        
    }
    
    
    @IBAction func buttonPressed2(_ sender: UIButton) {
        print("STOP Button pressed")
        
        sendStopToPeripheral()
    }
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        print("Button pressed")
        
        // Start scanning for Bluetooth devices if Bluetooth is turned on
        if centralManager.state == .poweredOn {
            centralManager.scanForPeripherals(withServices: nil, options: nil)
            print("Scanning for Bluetooth devices...")
        } else {
            print("Bluetooth is not turned on")
        }
    }
    

    @IBAction func navigateToMapViewController(_ sender: UIButton) {
        if let mapViewController = storyboard?.instantiateViewController(withIdentifier: "MapVC") as? CustomMapViewController {
            mapViewController.delegate = self
            present(mapViewController, animated: true, completion: nil)
        }
        
        
    }
}


extension ViewController: MapViewControllerDelegate {
    func didChooseLocation(latitude: Double, longitude: Double) {
        print("Received coordinates: \(latitude), \(longitude)")

        let clippedLatitude = String(format: "%.6f", latitude)
        let clippedLongitude = String(format: "%.6f", longitude)
        // Create coordinate string
        let coordinateString = "\(clippedLatitude),\(clippedLongitude)"
        
        //print("Coordinate String: \(coordinateString)")
        let formattedCoordinate = convertCoordinatesToIntegerFormat(coordinateString)
        //print("Formatted Coordinate: \(formattedCoordinate)") // Output: 37338950,-121880688
        
        
        // After updating the coordinate
        GlobalCoordinateManager.shared.coordinate = formattedCoordinate
        print("Global coordinates updated: \(formattedCoordinate)")

        // Send updated coordinates
        sendCoordinatesToPeripheral()
        
    }
    func convertCoordinatesToIntegerFormat(_ coordinateString: String) -> String {
        // Split the coordinate string into latitude and longitude components
        let components = coordinateString.components(separatedBy: ",")
        
        guard components.count == 2, // Ensure both latitude and longitude are present
              let latitude = Double(components[0]),
              let longitude = Double(components[1]) else {
            return "" // Return empty string if the format is incorrect
        }
        
        // Remove decimal points and convert to integers
        let latitudeInt = Int(latitude * 1_000_000)
        let longitudeInt = Int(longitude * 1_000_000)
        
        // Concatenate the integer components
        let formattedCoordinate = "\(latitudeInt),\(longitudeInt)\n"
        
        return formattedCoordinate
    }
}


extension ViewController: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("In didDiscoverServices")
        
        if let error = error {
            print("Error discovering services: \(error.localizedDescription)")
            return
        }
        
        guard let services = peripheral.services else {
            print("No services discovered")
            return
        }
        
        for service in services {
            print("Discovered service: \(service.uuid)")
            
            // Discover characteristics of each service
            peripheral.discoverCharacteristics(nil, for: service)
        }
        
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let error = error {
            print("Error discovering characteristics: \(error.localizedDescription)")
            return
        }
        guard let characteristics = service.characteristics else {
            print("No characteristics discovered for service: \(service.uuid)")
            return
        }
        
        for characteristic in characteristics {
            print("Discovered characteristic: \(characteristic.uuid)")
            
            // Check if this characteristic supports write operations
            if characteristic.properties.contains(.write) || characteristic.properties.contains(.writeWithoutResponse) {
                writeCharacteristic = characteristic
                print("Write characteristic found: \(characteristic.uuid)")
                
            }
            
            if characteristic.uuid == CBUUID(string: "6E400003-B5A3-F393-E0A9-E50E24DCCA9E") {
                readCharacteristic = characteristic
                print("Read characteristic found: \(characteristic.uuid)")
                
                // Now that we have the read characteristic, we can read data
                peripheral.setNotifyValue(true, for: characteristic) // Enable notifications for this characteristic
                peripheral.readValue(for: characteristic) // Read the initial value
                print("Reading data...")
            }
            
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {

        if let error = error {
            print("Error updating value for characteristic: \(error.localizedDescription)")
            return
        }
        
        if let value = characteristic.value, let message = String(data: value, encoding: .utf8) {
            //print("Message received: \(message)")

            DispatchQueue.main.async {
                self.messageLabel.text = message
            }
        }
    }
}


extension ViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("Bluetooth is On")
        case .poweredOff:
            print("Bluetooth is Off")
        case .resetting:
            print("Bluetooth is Resetting")
        case .unauthorized:
            print("Bluetooth not authorized")
        case .unsupported:
            print("Bluetooth not supported")
        case .unknown:
            print("Bluetooth unknown state")
        @unknown default:
            print("A new case was added that is not handled")
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if let name = peripheral.name, name != "Unknown Device" {
            // Avoid adding duplicates
            if !discoveredDevices.contains(where: { $0.identifier == peripheral.identifier }) {
                discoveredDevices.append(peripheral)
                DispatchQueue.main.async { [weak self] in self?.displayContent.reloadData()
                }
            }
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to peripheral: \(peripheral)")
        
        selectedPeripheral = peripheral // Make sure to store the reference to the connected didFailToConnectperipheral
        peripheral.delegate = self // Set the delegate to self for handling peripheral events
        
        // Start discovering services once connected
        peripheral.discoverServices(nil)
    
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected from peripheral: \(peripheral)")
        if let error = error {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Failed to connect to peripheral: \(peripheral), error: \(error?.localizedDescription ?? "Unknown error")")
        
        // Present an alert to the user with the option to try connecting again
        let alert = UIAlertController(title: "Connection Failed", message: "Failed to connect to the selected device. Would you like to try again?", preferredStyle: .alert)
        
        // Add a "Try Again" action to the alert
        alert.addAction(UIAlertAction(title: "Try Again", style: .default) { _ in
            // Retry connecting to the selected peripheral
            central.connect(peripheral, options: nil)
        })
        
        // Add a "Cancel" action to the alert
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // Present the alert
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to reuse a cell or create a new one if necessary
        let cellIdentifier = "DeviceCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) ?? UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        
        // Set the cell's text to the name of the peripheral or "Unknown Device"
        let peripheral = discoveredDevices[indexPath.row]
        cell.textLabel?.text = peripheral.name ?? "Unknown Device"
        
        return cell
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Handle cell selection
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedPeripheral = discoveredDevices[indexPath.row]
        // You can perform some action with the selected peripheral here
        
        if selectedPeripheral.name != "Unknown Device" {
            print("Selected device: \(selectedPeripheral.name ?? "Unknown Device")")
        }
        
        // Attempt to connect to the selected peripheral
        centralManager.connect(selectedPeripheral, options: nil)
        
    }
}


extension ViewController {
    func sendCoordinatesToPeripheral() {
        guard let peripheral = selectedPeripheral,
              let characteristic = writeCharacteristic,
              characteristic.properties.contains(.write) || characteristic.properties.contains(.writeWithoutResponse),
              let globalCoordinateString = GlobalCoordinateManager.shared.coordinate.data(using: .utf8) else {
            print("Cannot send data: Peripheral or characteristic not set up correctly.")
            return
        }

        if characteristic.uuid == CBUUID(string: "6E400002-B5A3-F393-E0A9-E50E24DCCA9E") {
            //writeCharacteristic = characteristic
            print("Adafruit BLE write characteristic FOUND")
            peripheral.writeValue(globalCoordinateString, for: characteristic, type: .withResponse)
            print("GPS DESTINATION coordinates sent: \(GlobalCoordinateManager.shared.coordinate)")
        }
        else{
            print("Adafruit BLE write characteristic NOT FOUND")
        }
    }
}


extension ViewController {
    func sendStopToPeripheral() {
        guard let peripheral = selectedPeripheral,
              let characteristic = writeCharacteristic,
              characteristic.properties.contains(.write) || characteristic.properties.contains(.writeWithoutResponse),
              let stopData = "S".data(using: .utf8),
              let stringRepresentation = String(data: stopData, encoding: .utf8) else {
                print("Cannot send data: Peripheral or characteristic not set up correctly.")
            return
          
        }

        
        peripheral.writeValue(stopData, for: characteristic, type: .withResponse) // or .withoutResponse
        print("stopData sent: \(stringRepresentation)")
//        
        
        if characteristic.uuid == CBUUID(string: "6E400002-B5A3-F393-E0A9-E50E24DCCA9E") {
            //writeCharacteristic = characteristic
            print("Adafruit BLE write characteristic FOUND")
            peripheral.writeValue(stopData, for: characteristic, type: .withResponse) // or .withoutResponse
            print("stopData sent: \(stopData)")
            print("stopData sent str: \(stringRepresentation)")
        }
        else{
            print("Adafruit BLE write characteristic NOT FOUND")
        }
    }
}
