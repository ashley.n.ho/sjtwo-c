#include <stdlib.h>

#include "driver__msg_bank.h"
#include "driver_logic.h"

#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include "project.h"

static dbc_SENSOR_SONARS_s sensor_data = {};
static dbc_GEO_STATUS_s geo_status = {};
static dbc_MOTOR_STOP_s motor_stop = {};
static dbc_MOTOR_CMD_s motor_data = {};

const uint32_t dbc_mia_threshold_SENSOR_SONARS = 150;
const dbc_SENSOR_SONARS_s dbc_mia_replacement_SENSOR_SONARS = {
    .SENSOR_SONARS_left = 100, .SENSOR_SONARS_middle = 100, .SENSOR_SONARS_right = 100};

dbc_SENSOR_SONARS_s driver_msg_bank__get_sensor_data(void) { return sensor_data; }

dbc_GEO_STATUS_s driver_msg_bank__get_geo_data(void) { return geo_status; }

dbc_MOTOR_STOP_s driver_msg_bank__get_motor_status(void) { return motor_stop; }

void driver_msg_bank__transmit_msg(void) {
  motor_data = driver__get_motor_commands();
  dbc_encode_and_send_MOTOR_CMD(NULL, &motor_data);
}

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  can__msg_t can_msg = {};

  can_msg.msg_id = message_id;
  can_msg.frame_fields.data_len = dlc;
  memcpy(can_msg.data.bytes, bytes, sizeof(can_msg.data.bytes));

  return (can__tx(can1, &can_msg, 0));
}

void driver_msg_bank__receive_msg(dbc_message_header_t header, const uint8_t bytes[8]) {
  dbc_decode_SENSOR_SONARS(&sensor_data, header, bytes);
  dbc_decode_GEO_STATUS(&geo_status, header, bytes);
  dbc_decode_MOTOR_STOP(&motor_stop, header, bytes);
}

void driver_msg_bank__handle_msg(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    // Construct "message header" that we need for the decode_*() API
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    driver_msg_bank__receive_msg(header, can_msg.data.bytes);
  }
}

// MIA management:
void driver_msg_bank__service_mia_10hz(void) {

  const uint32_t mia_increment_value = 100;

  sensor_data = driver_msg_bank__get_sensor_data();
  if (dbc_service_mia_SENSOR_SONARS(&sensor_data, mia_increment_value)) {
    // Turn off LED to indicate sonar sensor MIA
    gpio__set(board_io__get_led0()); // in future use LCD
  }
}