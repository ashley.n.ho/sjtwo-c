#pragma once

#include "can_bus.h"
#include "project.h"

// Invoke this method in your periodic callbacks
void driver_msg_bank__handle_msg(void);
void driver_msg_bank__transmit_msg(void);
void driver_msg_bank__receive_msg(dbc_message_header_t header, const uint8_t bytes[8]);

// MIA management:
void driver_msg_bank__service_mia_10hz(void);

// "GET" APIs to obtain latest data
dbc_SENSOR_SONARS_s driver_msg_bank__get_sensor_data(void);
dbc_GEO_STATUS_s driver_msg_bank__get_geo_data(void);
dbc_MOTOR_STOP_s driver_msg_bank__get_motor_stop(void);