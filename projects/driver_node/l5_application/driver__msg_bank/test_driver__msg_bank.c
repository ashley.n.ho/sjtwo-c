#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockdriver_logic.h"
#include "Mockgpio.h"
#include "project.h"

// Include the source we wish to test
#include "driver__msg_bank.h"

void setUp(void) {}

void test_trasnmit_message(void) {
  dbc_MOTOR_CMD_s motor_data;
  driver__get_motor_commands_ExpectAndReturn(motor_data);
  can__tx_ExpectAnyArgsAndReturn(1);

  driver_msg_bank__transmit_msg();
}

void test_msg_bank_sensor_sonars(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_SENSOR_SONARS.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  can__rx_ExpectAnyArgsAndReturn(0);

  driver_msg_bank__handle_msg();
}

void test_msg_bank_geo_status(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_GEO_STATUS.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  can__rx_ExpectAnyArgsAndReturn(0);

  driver_msg_bank__handle_msg();
}

void test_msg_bank_motor_stop(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_MOTOR_STOP.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  can__rx_ExpectAnyArgsAndReturn(0);

  driver_msg_bank__handle_msg();
}

void test_can_handler__manage_mia_10hz(void) {
  dbc_SENSOR_SONARS_s sensor_msg;

  driver_msg_bank__service_mia_10hz();
}