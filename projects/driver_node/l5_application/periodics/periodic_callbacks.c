#include "periodic_callbacks.h"
#include "board_io.h"
#include "gpio.h"
#include <stdio.h>

#include "can_bus_initializer.h"
#include "driver__msg_bank.h"
#include "driver_logic.h"
#include "i2c_lcd.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_bus_initializer_run_once(can1, 100, 200, 200);
  i2c_lcd__init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Add your code here
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Add your code here
  driver_msg_bank__handle_msg();
  driver_msg_bank__service_mia_10hz();
  driver_msg_bank__transmit_msg();
  i2c_lcd__clear();
  driver__lcd_display_data();
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Add your code here
  driver__process_input();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}