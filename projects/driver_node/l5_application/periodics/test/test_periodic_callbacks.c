#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus_initializer.h"
#include "Mockdriver__msg_bank.h"
#include "Mockdriver_logic.h"
#include "Mockgpio.h"
#include "Mocki2c_lcd.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer_run_once_ExpectAnyArgsAndReturn(1);
  i2c_lcd__init_ExpectAndReturn(1);

  periodic_callbacks__initialize();
}

void test__periodic_callbacks__10Hz(void) {
  driver_msg_bank__handle_msg_Expect();
  driver_msg_bank__service_mia_10hz_Expect();
  driver_msg_bank__transmit_msg_Expect();
  i2c_lcd__clear_Expect();
  driver__lcd_display_data_Expect();

  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  driver__process_input_Expect();

  periodic_callbacks__100Hz(0);
}