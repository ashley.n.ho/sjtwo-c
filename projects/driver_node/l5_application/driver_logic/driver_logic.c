#include "driver_logic.h"
#include "driver__msg_bank.h"

#include "board_io.h"
#include "gpio.h"
#include "i2c_lcd.h"
#include "project.h"
#include <stdio.h>

typedef struct {
  bool LEFT;
  bool MIDDLE;
  bool RIGHT;
} obstacle_below_threshold_s;

static uint8_t turn_threshold = 35;                 // adjust distance with field testing
static uint8_t middle_sensor_danger_threshold = 50; // if something is directly in front

static dbc_SENSOR_SONARS_s local_sensor_data = {};
static dbc_GEO_STATUS_s local_geo_status = {};
static dbc_MOTOR_STOP_s local_motor_stop = {};
static dbc_MOTOR_CMD_s motor_commands = {};

// slight turns for destination movement
static void driver__go_to_destination(void) {
  int angle_difference = local_geo_status.GEO_STATUS_COMPASS_BEARING - local_geo_status.GEO_STATUS_COMPASS_HEADING;

  if (local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION > 2) {
    if (angle_difference > 0) {
      // go right
      motor_commands.MOTOR_CMD_steer = 1;
      motor_commands.MOTOR_CMD_forward = 3;
    } else if (angle_difference < 0) {
      // go left
      motor_commands.MOTOR_CMD_steer = -1;
      motor_commands.MOTOR_CMD_forward = 3;
    } else { // go straight
      motor_commands.MOTOR_CMD_steer = 0;
      motor_commands.MOTOR_CMD_forward = 3;
    }
  }

  else { // reached destination
    motor_commands.MOTOR_CMD_forward = 0;
    motor_commands.MOTOR_CMD_steer = 0;
  }
}

static obstacle_below_threshold_s driver__closest_obstacle(void) {
  obstacle_below_threshold_s thresholds = {.LEFT = false, .MIDDLE = false, .RIGHT = false};
  if (local_sensor_data.SENSOR_SONARS_left < turn_threshold) {
    thresholds.LEFT = true;
  }
  if (local_sensor_data.SENSOR_SONARS_middle < middle_sensor_danger_threshold) {
    thresholds.MIDDLE = true;
  }
  if (local_sensor_data.SENSOR_SONARS_right < turn_threshold) {
    thresholds.RIGHT = true;
  }
  return thresholds;
}

static void driver__led_indicators(void) {
  // reverse indication
  if (motor_commands.MOTOR_CMD_forward == -1) {
    gpio__set(board_io__get_led0());
    gpio__reset(board_io__get_led1());
  } else if (motor_commands.MOTOR_CMD_forward == 3) {
    gpio__set(board_io__get_led0());
    gpio__set(board_io__get_led1());
  } else if (motor_commands.MOTOR_CMD_forward == 0) {
    gpio__reset(board_io__get_led0());
    gpio__set(board_io__get_led1());
  }

  // turning indication
  if (motor_commands.MOTOR_CMD_steer > 0) {
    gpio__reset(board_io__get_led2());
    gpio__set(board_io__get_led3());
  } else if (motor_commands.MOTOR_CMD_steer < 0) {
    gpio__set(board_io__get_led2());
    gpio__reset(board_io__get_led3());
  } else {
    gpio__set(board_io__get_led2());
    gpio__set(board_io__get_led3());
  }
}

static void driver__steering(void) {
  obstacle_below_threshold_s below_thresholds = driver__closest_obstacle();

  if (below_thresholds.MIDDLE && (below_thresholds.LEFT && below_thresholds.RIGHT)) {
    if (local_sensor_data.SENSOR_SONARS_left < local_sensor_data.SENSOR_SONARS_right) {
      motor_commands.MOTOR_CMD_forward = -1;
      motor_commands.MOTOR_CMD_steer = 2;
    } else {
      motor_commands.MOTOR_CMD_forward = -1;
      motor_commands.MOTOR_CMD_steer = -2;
    }
  } else if (below_thresholds.MIDDLE) {
    if (local_sensor_data.SENSOR_SONARS_left < local_sensor_data.SENSOR_SONARS_right) {
      motor_commands.MOTOR_CMD_steer = 2;
      motor_commands.MOTOR_CMD_forward = -1;
    } else {
      motor_commands.MOTOR_CMD_steer = -2;
      motor_commands.MOTOR_CMD_forward = -1;
    }
  } else if (below_thresholds.LEFT) {
    motor_commands.MOTOR_CMD_steer = 2;
    motor_commands.MOTOR_CMD_forward = 3;
  } else if (below_thresholds.RIGHT) {
    motor_commands.MOTOR_CMD_steer = -2;
    motor_commands.MOTOR_CMD_forward = 3;
  } else if (below_thresholds.LEFT && below_thresholds.RIGHT) {
    motor_commands.MOTOR_CMD_forward = -1;
    motor_commands.MOTOR_CMD_steer = -2;
  }
}

static void driver__avoid_obstacle(void) {
  obstacle_below_threshold_s below_thresholds = driver__closest_obstacle();

  if (!(below_thresholds.MIDDLE) && !(below_thresholds.LEFT) && !(below_thresholds.RIGHT)) {
#if (1)
    driver__go_to_destination();

// Include when testing without destination //
#else
    motor_commands.MOTOR_CMD_forward = 3;
    motor_commands.MOTOR_CMD_steer = 0;
#endif
  } else {
    driver__steering();
  }
  driver__led_indicators();
}

void driver__process_input(void) {
  local_sensor_data = driver_msg_bank__get_sensor_data();
  local_geo_status = driver_msg_bank__get_geo_data();
  local_motor_stop = driver_msg_bank__get_motor_stop();
}

void driver__lcd_display_data(void) {
  char debug_str[15];

  if (local_motor_stop.MOTOR_STOP_stop) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("   MOTOR STOPPED!");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf(" PLS RESTART BRIDGE");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("     TO CONTINUE");
  } else if (local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION) {
    sprintf(debug_str, "%.1lf", local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);

    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("   PROJECT TEAM X");

    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("Distance:");
    i2c_lcd__starting_position(1, 10);
    i2c_lcd__printf(debug_str);

    sprintf(debug_str, "%d", (int)local_geo_status.GEO_STATUS_COMPASS_HEADING);
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     Heading:");
    i2c_lcd__starting_position(2, 13);
    i2c_lcd__printf(debug_str);

    sprintf(debug_str, "%d", (int)local_geo_status.GEO_STATUS_COMPASS_BEARING);
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("     Bearing:");
    i2c_lcd__starting_position(3, 13);
    i2c_lcd__printf(debug_str);
  } else if (local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION < 2) {
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("DESTINATION REACHED!");
  }
}

// This should operate on the "static" struct instance of dbc_SENSOR_data_s to run the obstacle avoidance logic
dbc_MOTOR_CMD_s driver__get_motor_commands(void) {
  driver__avoid_obstacle();

  return motor_commands;
}