#pragma once

#include "project.h"

// This should copy data to its internal "static" struct instances
void driver__process_input(void);

// This should operate on the "static" struct instances of dbc_SENSOR_data_s to run the obstacle avoidance logic
dbc_MOTOR_CMD_s driver__get_motor_commands(void);

void driver__lcd_display_data(void);
