#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockdriver__msg_bank.h"
#include "Mockgpio.h"
#include "Mocki2c_lcd.h"
#include "project.h"

// Include the source we wish to test
#include "driver_logic.c"

void setUp(void) {
  motor_commands.MOTOR_CMD_forward = 0;
  motor_commands.MOTOR_CMD_steer = 0;
}

void test_driver__process_input(void) {
  dbc_SENSOR_SONARS_s sonar_data;
  sonar_data.SENSOR_SONARS_left = 40;
  sonar_data.SENSOR_SONARS_right = 60;
  sonar_data.SENSOR_SONARS_middle = 30;

  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_COMPASS_BEARING = 18;
  geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  dbc_MOTOR_STOP_s motor_stop;
  motor_stop.MOTOR_STOP_stop = 0;

  driver_msg_bank__get_sensor_data_ExpectAndReturn(sonar_data);
  driver_msg_bank__get_geo_data_ExpectAndReturn(geo_status);
  driver_msg_bank__get_motor_stop_ExpectAndReturn(motor_stop);

  driver__process_input();
  TEST_ASSERT_EQUAL(local_sensor_data.SENSOR_SONARS_left, sonar_data.SENSOR_SONARS_left);
  TEST_ASSERT_EQUAL(local_sensor_data.SENSOR_SONARS_middle, sonar_data.SENSOR_SONARS_middle);
  TEST_ASSERT_EQUAL(local_sensor_data.SENSOR_SONARS_right, sonar_data.SENSOR_SONARS_right);

  TEST_ASSERT_EQUAL(local_geo_status.GEO_STATUS_COMPASS_BEARING, geo_status.GEO_STATUS_COMPASS_BEARING);
  TEST_ASSERT_EQUAL(local_geo_status.GEO_STATUS_COMPASS_HEADING, geo_status.GEO_STATUS_COMPASS_HEADING);
  TEST_ASSERT_EQUAL(local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION, geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);

  TEST_ASSERT_EQUAL(local_motor_stop.MOTOR_STOP_stop, motor_stop.MOTOR_STOP_stop);
}

void test_driver__go_forward(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 36;
  local_sensor_data.SENSOR_SONARS_right = 36;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 18;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 0);
}

void test_driver__move_motor_left_obstacle(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 110;
  local_sensor_data.SENSOR_SONARS_right = 10;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 18;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, -2);
}

void test_driver__move_motor_right_obstacle(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 10;
  local_sensor_data.SENSOR_SONARS_right = 300;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 18;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 2);
}

void test_driver__move_motor_right_no_obstacle(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 36;
  local_sensor_data.SENSOR_SONARS_right = 36;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 20;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 1);
}

void test_driver__move_motor_left_no_obstacle(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 36;
  local_sensor_data.SENSOR_SONARS_right = 36;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  dbc_GEO_STATUS_s geo_status;
  local_geo_status.GEO_STATUS_COMPASS_BEARING = 12;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 13;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 400;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, -1);
}

void test_driver_reached_destination(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 101;
  local_sensor_data.SENSOR_SONARS_right = 101;
  local_sensor_data.SENSOR_SONARS_middle = 101;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 12;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 13;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 0;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 0);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 0);
}

void test_driver__move_motor_until_obstacle_clear(void) {
  gpio_s gpio = {};

  local_sensor_data.SENSOR_SONARS_left = 25;
  local_sensor_data.SENSOR_SONARS_right = 36;
  local_sensor_data.SENSOR_SONARS_middle = 51;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 12;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 10;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 100;

  for (int i = 0; i < 9; i++) {
    local_sensor_data.SENSOR_SONARS_left++;
    board_io__get_led0_ExpectAndReturn(gpio);
    gpio__set_Expect(gpio);
    board_io__get_led1_ExpectAndReturn(gpio);
    gpio__set_Expect(gpio);
    board_io__get_led2_ExpectAndReturn(gpio);
    gpio__reset_Expect(gpio);
    board_io__get_led3_ExpectAndReturn(gpio);
    gpio__set_Expect(gpio);

    driver__get_motor_commands();
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 2);
  }
  local_sensor_data.SENSOR_SONARS_left++;

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 1);
}

void test_driver__go_backward(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 10;
  local_sensor_data.SENSOR_SONARS_right = 9;
  local_sensor_data.SENSOR_SONARS_middle = 31;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 12;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 10;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 100;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, -1);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, -2);
}

void test_driver__middle_sensor_below_threshold(void) {
  gpio_s gpio = {};

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  local_sensor_data.SENSOR_SONARS_left = 40;
  local_sensor_data.SENSOR_SONARS_right = 50;
  local_sensor_data.SENSOR_SONARS_middle = 19;

  local_geo_status.GEO_STATUS_COMPASS_BEARING = 12;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 10;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 100;

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, -1);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 2);

  local_sensor_data.SENSOR_SONARS_left = 50;
  local_sensor_data.SENSOR_SONARS_right = 40;

  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  driver__get_motor_commands();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, -1);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, -2);
}

void test_driver__turn_right_until_angle_difference_zero(void) {
  local_geo_status.GEO_STATUS_COMPASS_BEARING = 8;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 18;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 100;

  for (int i = 0; i < 10; i++) {
    driver__go_to_destination();
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, -1);

    local_geo_status.GEO_STATUS_COMPASS_HEADING -= 1;
  }

  driver__go_to_destination();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 0);
}

void test_driver__turn_left_until_angle_difference_zero(void) {
  local_geo_status.GEO_STATUS_COMPASS_BEARING = 18;
  local_geo_status.GEO_STATUS_COMPASS_HEADING = 1;
  local_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 100;

  for (int i = 0; i < 17; i++) {
    driver__go_to_destination();
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
    TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 1);

    local_geo_status.GEO_STATUS_COMPASS_HEADING += 1;
  }

  driver__go_to_destination();
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_forward, 3);
  TEST_ASSERT_EQUAL(motor_commands.MOTOR_CMD_steer, 0);
}