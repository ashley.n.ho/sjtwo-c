#include "i2c_lcd.h"

#include "delay.h"
#include "i2c.h"

static const i2c_e i2c_lcd__sensor_bus = I2C__2;
static const uint8_t i2c_lcd__address = 0x4E;

bool i2c_lcd__init(void) {

  // 4 bit initialisation
  delay__ms(50);
  i2c_lcd__send_command(0x30);
  delay__ms(5);
  i2c_lcd__send_command(0x30);
  delay__ms(1);
  i2c_lcd__send_command(0x30);
  delay__ms(10);
  i2c_lcd__send_command(0x20); // 4bit mode
  delay__ms(10);

  // dislay initialisation
  i2c_lcd__send_command(0x28); // Function set
  delay__ms(1);
  i2c_lcd__send_command(0x08); // Display off
  delay__ms(1);
  i2c_lcd__send_command(0x01); // clear
  delay__ms(1);
  delay__ms(1);
  i2c_lcd__send_command(0x06); // Entry mode set
  delay__ms(1);
  i2c_lcd__send_command(0x0C); // Display on

  return i2c__detect(I2C__2, 0x4E);
}

void i2c_lcd__send_command(char command) {

  uint8_t cmd_w[4];
  cmd_w[0] = (command & 0xf0) | 0x0C;
  cmd_w[1] = (command & 0xf0) | 0x08;
  cmd_w[2] = ((command << 4) & 0xf0) | 0x0C;
  cmd_w[3] = ((command << 4) & 0xf0) | 0x08;
  i2c__write_single(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[0], cmd_w[1]);
  i2c__write_single(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[2], cmd_w[3]);
}

void i2c_lcd__send_data(char data) {

  uint8_t data_w[4];
  data_w[0] = (data & 0xf0) | 0x0D;
  data_w[1] = (data & 0xf0) | 0x09;
  data_w[2] = ((data << 4) & 0xf0) | 0x0D;
  data_w[3] = ((data << 4) & 0xf0) | 0x09;

  i2c__write_single(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[0], data_w[1]);
  i2c__write_single(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[2], data_w[3]);
}

void i2c_lcd__clear(void) {
  i2c_lcd__send_command(0x00);
  for (int i = 0; i < 100; i++) {
    i2c_lcd__send_data(' ');
  }
}

void i2c_lcd__printf(char *line) {
  while (*line)
    i2c_lcd__send_data(*line++);
}

void i2c_lcd__starting_position(uint8_t y, uint8_t x) {

  char Starting_Addr = 0x80;
  switch (y) {
  case 0:
    Starting_Addr |= 0x00;
    break;
  case 1:
    Starting_Addr |= 0x40;
    break;
  case 2:
    Starting_Addr |= 0x14;
    break;
  case 3:
    Starting_Addr |= 0x54;
    break;
  default:;
  }

  Starting_Addr += x;

  i2c_lcd__send_command(0x80 | Starting_Addr);
}
