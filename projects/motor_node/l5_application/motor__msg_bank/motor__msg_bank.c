#include "motor__msg_bank.h"

#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include "project.h"

static dbc_MOTOR_CMD_s motor_cmd = {};
static dbc_MOTOR_STOP_s stop_motor = {};

const uint32_t dbc_mia_threshold_MOTOR_CMD = 150;
const uint32_t dbc_mia_threshold_MOTOR_STOP = 150;

const dbc_MOTOR_CMD_s dbc_mia_replacement_MOTOR_CMD = {.MOTOR_CMD_steer = 0, .MOTOR_CMD_forward = 0};
const dbc_MOTOR_STOP_s dbc_mia_replacement_MOTOR_STOP = {.MOTOR_STOP_stop = 0};

dbc_MOTOR_CMD_s motor_msg_bank__get_motor_cmd(void) { return motor_cmd; }

dbc_MOTOR_STOP_s motor_msg_bank__get_motor_stop(void) { return stop_motor; }

void motor_msg_bank__handle_msg(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    // Construct "message header" that we need for the decode_*() API
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    motor_msg_bank__receive_motor_command(header, can_msg.data.bytes);
  }
}

void motor_msg_bank__receive_motor_command(dbc_message_header_t header, const uint8_t bytes[8]) {
  dbc_decode_MOTOR_CMD(&motor_cmd, header, bytes);
  dbc_decode_MOTOR_STOP(&stop_motor, header, bytes);
}

// MIA management:
void motor_msg_bank__service_mia_10hz(void) {

  const uint32_t mia_increment_value = 100;

  motor_cmd = motor_msg_bank__get_motor_cmd();
  if (dbc_service_mia_MOTOR_CMD(&motor_cmd, mia_increment_value)) {
    // Turn off LED to indicate sonar sensor MIA
    gpio__set(board_io__get_led0()); // in future use LCD
  }
}