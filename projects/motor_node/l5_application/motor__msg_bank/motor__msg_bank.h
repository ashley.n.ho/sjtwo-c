#pragma once

#include "can_bus.h"
#include "project.h"

void motor_msg_bank__handle_msg(void);
void motor_msg_bank__receive_motor_command(dbc_message_header_t header, const uint8_t bytes[8]);
dbc_MOTOR_CMD_s motor_msg_bank__get_motor_cmd(void);
dbc_MOTOR_STOP_s motor_msg_bank__get_motor_stop(void);

void motor_msg_bank__service_mia_10hz(void);
