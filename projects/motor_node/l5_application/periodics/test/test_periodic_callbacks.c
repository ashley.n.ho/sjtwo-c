#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus_initializer.h"
#include "Mockgpio.h"
#include "Mockmotor__msg_bank.h"
#include "Mockmotor__pwm.h"
#include "Mockmotor_rpm_driver.h"
#include "Mockmotor_speed_pid.h"
// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  gpio_s led;
  board_io__get_led3_ExpectAndReturn(led);
  gpio__set_ExpectAnyArgs();
  can_bus_initializer__run_once_ExpectAnyArgsAndReturn(1);
  motor_pwm__init_Expect();
  motor_rpm_driver_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) { periodic_callbacks__1Hz(0); }

void test__periodic_callbacks__10Hz(void) {
  for (int i = 0; i < 50; i++) {

    motor_msg_bank__handle_msg_Expect();
    motor_msg_bank__service_mia_10hz_Expect();
    motor_pwm__set_pwm_Expect();

    if (i % 7 == 0) {
      motor_rpm_driver_calc_speed_Expect();
    }
    motor_speed_pid_run_once_Expect();
    periodic_callbacks__10Hz(i);
  }
}

void test__periodic_callbacks__100Hz(void) {

  motor_pwm__process_input_Expect();

  periodic_callbacks__100Hz(0);
}