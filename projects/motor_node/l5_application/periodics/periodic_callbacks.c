#include "periodic_callbacks.h"

#include "board_io.h"
#include "can_bus_initializer.h"
#include "gpio.h"
#include "motor__msg_bank.h"
#include "motor__pwm.h"
#include "motor_rpm_driver.h"
#include "motor_speed_pid.h"
#include <stdio.h>

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  gpio__set(board_io__get_led3());
  motor_rpm_driver_init();
  can_bus_initializer__run_once(can1, 100, 200, 200);
  motor_pwm__init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Add your code here
  motor_msg_bank__handle_msg();
  motor_msg_bank__service_mia_10hz();
  motor_pwm__set_pwm();
  if (callback_count % 7 == 0) {
    motor_rpm_driver_calc_speed();
  }
  motor_speed_pid_run_once();
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Add your code here
  motor_pwm__process_input();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) { motor_rpm_driver_read_pulse(); }