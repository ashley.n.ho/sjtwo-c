#pragma once

void motor_rpm_driver_init(void);

float motor_rpm_driver_get_speed(void);

/// @return speed in meters/second
void motor_rpm_driver_calc_speed(void);

/// @brief should be called in 1khz
void motor_rpm_driver_read_pulse(void);