#include "motor_rpm_driver.h"
#include "can_bus.h"
#include "gpio.h"
#include "project.h"
#include "sys_time.h"
#include <stdio.h>

// 4.5 inch wheel diameter
#define WHEEL_DIAMETER_CM 11.43

// Use this time interval to calculate speed
#define TIME_DELTA_MS 700

static const gpio_s rpm_pulse_pin = {GPIO__PORT_0, 6};

static bool gpio_prev_val = 1;
static uint64_t pulse_count;
static uint64_t prev_pulse_count;

static float rc_car_speed;

void motor_rpm_driver_init(void) { gpio__construct_as_input(GPIO__PORT_0, 6); }

float motor_rpm_driver_get_speed(void) { return rc_car_speed; }

static uint64_t prev_time = 0;

void motor_rpm_driver_calc_speed(void) {

  /*uint64_t curr_time = sys_time__get_uptime_ms();
  printf("delta: %d\n", (int)curr_time - (int)prev_time);
  prev_time = curr_time;*/

  uint64_t pulse_delta = pulse_count - prev_pulse_count;
  prev_pulse_count = pulse_count;

  float distance_meters = (pulse_delta * WHEEL_DIAMETER_CM) / 100.0;
  float speed = distance_meters / ((float)TIME_DELTA_MS / (float)1000.0);
  rc_car_speed = speed;

  // printf("speed: %f\n", rc_car_speed);
  static dbc_DBG_MSG_MOTOR_s speed_dbg;
  speed_dbg.SPEED_M_PER_S = rc_car_speed;
  can__msg_t can_msg = {};
  dbc_message_header_t header;

  header = dbc_encode_DBG_MSG_MOTOR(can_msg.data.bytes, &speed_dbg);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  if (!can__tx(can1, &can_msg, 0)) {
    printf("can tx: Error!\n");
  }
}

// should be called in 1khz
void motor_rpm_driver_read_pulse(void) {
  bool curr_val = gpio__get(rpm_pulse_pin);
  if (curr_val != gpio_prev_val && !curr_val) {
    pulse_count++;
  }
  gpio_prev_val = curr_val;
}