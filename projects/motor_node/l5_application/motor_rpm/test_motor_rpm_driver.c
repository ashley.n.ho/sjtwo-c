#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "motor_rpm_driver.c"

void setUp(void) {}

void tearDown(void) {}

void test_motor_rpm_driver_init(void) {
  gpio_s rpm_pin = {GPIO__PORT_0, 6};
  gpio__construct_as_input_ExpectAnyArgsAndReturn(rpm_pin);
  motor_rpm_driver_init();
}

void test_motor_rpm_driver_calc_speed_1_pulse(void) {
  pulse_count = 1;
  prev_pulse_count = 0;

  can__tx_ExpectAnyArgsAndReturn(1);

  motor_rpm_driver_calc_speed();

  float speed = motor_rpm_driver_get_speed();
  TEST_ASSERT_EQUAL_FLOAT(0.1632857, speed);
}

void test_motor_rpm_driver_calc_speed_5_pulse(void) {
  pulse_count = 5;
  prev_pulse_count = 0;

  can__tx_ExpectAnyArgsAndReturn(1);
  motor_rpm_driver_calc_speed();

  float speed = motor_rpm_driver_get_speed();
  TEST_ASSERT_EQUAL_FLOAT(0.8164286, speed);
}

void test_motor_rpm_driver_read_pulse_low(void) {
  gpio_s pin = {GPIO__PORT_0, 6};
  gpio_prev_val = 1;
  pulse_count = 0;
  gpio__get_ExpectAndReturn(pin, 0);

  motor_rpm_driver_read_pulse();

  TEST_ASSERT_EQUAL(1, pulse_count);
}

void test_motor_rpm_driver_read_pulse_high(void) {
  gpio_s pin = {GPIO__PORT_0, 6};
  gpio_prev_val = 0;
  pulse_count = 0;
  gpio__get_ExpectAndReturn(pin, 1);

  motor_rpm_driver_read_pulse();

  TEST_ASSERT_EQUAL(0, pulse_count);
}