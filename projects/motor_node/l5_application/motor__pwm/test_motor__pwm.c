#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockmotor__msg_bank.h"
#include "Mockmotor_speed_pid.h"
#include "Mockpwm1.h"
#include "Mocksys_time.h"
#include "project.h"

// Include the source we wish to test
#include "motor__pwm.c"

void setUp(void) {
  ignore_cmds = false;
  reverse_flag = false;
  motor_cmd_local.MOTOR_CMD_forward = 0;
  motor_cmd_local.MOTOR_CMD_steer = 0;
  motor_stop_local.MOTOR_STOP_stop = 0;
}

void tearDown(void) {}

void test_turn_max_right_and_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_cmd_local.MOTOR_CMD_steer = 2;

  pwm1__set_duty_cycle_Expect(servo_pwm, 19.00);
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_turn_slight_right_and_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_cmd_local.MOTOR_CMD_steer = 1;

  pwm1__set_duty_cycle_Expect(servo_pwm, 17.00);
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_turn_max_left_and_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_cmd_local.MOTOR_CMD_steer = -2;

  pwm1__set_duty_cycle_Expect(servo_pwm, 11.00);
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_turn_slight_left_and_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_cmd_local.MOTOR_CMD_steer = -1;

  pwm1__set_duty_cycle_Expect(servo_pwm, 13.00);
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_go_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_cmd_local.MOTOR_CMD_steer = 2;

  pwm1__set_duty_cycle_Expect(servo_pwm, 19.00);
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_go_backward_and_left(void) {
  motor_cmd_local.MOTOR_CMD_forward = -1;
  motor_cmd_local.MOTOR_CMD_steer = -2;
  reverse_flag = true;

  pwm1__set_duty_cycle_Expect(servo_pwm, 11.00);
  motor_speed_pid_set_target_pwm_Expect(14.5);

  motor_pwm__set_servo_pwm();
  motor_pwm__set_motor_pwm();
}

void test_stop(void) {
  motor_cmd_local.MOTOR_CMD_forward = 0;

  motor_speed_pid_set_target_pwm_Expect(15.02);

  motor_pwm__set_motor_pwm();
}

void test_forward_then_backward(void) {
  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_speed_pid_set_target_pwm_Expect(15.8);

  motor_pwm__set_motor_pwm();

  motor_cmd_local.MOTOR_CMD_forward = -1;
  motor_speed_pid_set_target_pwm_Expect(15.02);
  sys_time__get_uptime_ms_ExpectAndReturn(0);

  motor_pwm__set_motor_pwm();
}

void test_backward_then_forward(void) {
  motor_cmd_local.MOTOR_CMD_forward = -1;
  reverse_flag = true;
  motor_speed_pid_set_target_pwm_Expect(14.5);

  motor_pwm__set_motor_pwm();

  motor_cmd_local.MOTOR_CMD_forward = 3;
  motor_speed_pid_set_target_pwm_Expect(15.02);
  sys_time__get_uptime_ms_ExpectAndReturn(0);

  motor_pwm__set_motor_pwm();
}

void test_mobile_app_stop(void) {

  motor_stop_local.MOTOR_STOP_stop = 1;
  motor_speed_pid_set_target_pwm_Expect(15.02);

  motor_pwm__set_motor_pwm();
}