#pragma once

// will determine current speed in kph/mph and fix error from target speed
// target speed set globally from "motor_speed_pid_set_target_pwm"
// Will be invoked from periodic_callbacks.c
void motor_speed_pid_run_once(void);

// convert PWM to a speed in kph/mph
// PID controller will determine current speed and fix error
// For the PWM constants that we set in motor__pwm.c, we need to figure out the speed in kph/mph to set here
void motor_speed_pid_set_target_pwm(float target_pwm);

/*TODO
For the PWM values we SET (only the ones we set),
determine the SPEED in kph/mph
Determine MIN/MAX PWM that we can set
*/