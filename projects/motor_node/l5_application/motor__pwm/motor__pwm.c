#include "motor__pwm.h"

#include "delay.h"
#include "gpio.h"
#include "motor__msg_bank.h"
#include "motor_speed_pid.h"
#include "project.h"
#include "pwm1.h"
#include "sys_time.h"
#include <stdio.h>

#define DIRECTION_CHANGE_DELAY 1000

static dbc_MOTOR_CMD_s motor_cmd_local = {};
static dbc_MOTOR_STOP_s motor_stop_local = {};

static const pwm1_channel_e servo_pwm = PWM1__2_1;
static const pwm1_channel_e motor_pwm = PWM1__2_0;

static const float servo_pwm_idle_duty_cycle = 15.48;
static const float motor_pwm_idle_duty_cycle = 15.02;

static bool reverse_flag = false;
static bool ignore_cmds = false;
static uint64_t start_time;

// -1 back
// 0 stop
// 3 is forward

static void motor_pwm__set_servo_pwm(void) {

  static float set_servo_duty_cycle = servo_pwm_idle_duty_cycle;

  if (motor_cmd_local.MOTOR_CMD_steer == 0) {
    // idle
    set_servo_duty_cycle = servo_pwm_idle_duty_cycle;
  } else if (motor_cmd_local.MOTOR_CMD_steer == 1) {
    // slightly right
    set_servo_duty_cycle = 17.00;
  } else if (motor_cmd_local.MOTOR_CMD_steer == 2) {
    // max right
    set_servo_duty_cycle = 19.00;
  } else if (motor_cmd_local.MOTOR_CMD_steer == -1) {
    // slightly left
    set_servo_duty_cycle = 13.00;
  } else if (motor_cmd_local.MOTOR_CMD_steer == -2) {
    // max left
    set_servo_duty_cycle = 11.00;
  }

  pwm1__set_duty_cycle(servo_pwm, set_servo_duty_cycle);
}

static void motor_pwm__set_motor_pwm(void) {
  static float set_motor_duty_cycle = motor_pwm_idle_duty_cycle;

  if (motor_stop_local.MOTOR_STOP_stop) {
    motor_speed_pid_set_target_pwm(motor_pwm_idle_duty_cycle);
  } else {
    if (!ignore_cmds) {
      if (motor_cmd_local.MOTOR_CMD_forward == -1) { // Reverse
        set_motor_duty_cycle = 14.50;
        if (reverse_flag == false) {
          motor_speed_pid_set_target_pwm(motor_pwm_idle_duty_cycle);
          ignore_cmds = true;
          start_time = sys_time__get_uptime_ms();
          reverse_flag = true;
          return;
        }

      } else if (motor_cmd_local.MOTOR_CMD_forward == 0) { // Stop
        set_motor_duty_cycle = motor_pwm_idle_duty_cycle;
      } else if (motor_cmd_local.MOTOR_CMD_forward == 3) { // Forward
        set_motor_duty_cycle = 15.8;

        if (reverse_flag == true) {
          motor_speed_pid_set_target_pwm(motor_pwm_idle_duty_cycle);
          ignore_cmds = true;
          start_time = sys_time__get_uptime_ms();
          reverse_flag = false;
          return;
        }
      }
      motor_speed_pid_set_target_pwm(set_motor_duty_cycle);
    } else if (sys_time__get_uptime_ms() - start_time >= DIRECTION_CHANGE_DELAY) {
      ignore_cmds = false;
    } else {
    }
  }
}

void motor_pwm__init(void) {

  const uint32_t default_pwm_frequency_hz = 100;

  // SERVO init
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
  pwm1__init_single_edge(default_pwm_frequency_hz);
  pwm1__set_duty_cycle(servo_pwm, servo_pwm_idle_duty_cycle);

  // MOTOR init
  gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1);
  pwm1__init_single_edge(default_pwm_frequency_hz);
  pwm1__set_duty_cycle(motor_pwm, motor_pwm_idle_duty_cycle);
  delay__ms(500);
}

void motor_pwm__process_input(void) {
  motor_cmd_local = motor_msg_bank__get_motor_cmd();
  motor_stop_local = motor_msg_bank__get_motor_stop();
}

void motor_pwm__set_pwm(void) {
  motor_pwm__set_motor_pwm();
  motor_pwm__set_servo_pwm();
}