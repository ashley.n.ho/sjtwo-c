#include "motor_speed_pid.h"
#include "board_io.h"
#include "gpio.h"
#include "motor_rpm_driver.h"
#include "pwm1.h"

#include <stdio.h>

// Constants for PID control
#define KP 3.5f // Proportional gain
#define KI 0.0f // 3.0  // Integral gain
#define KD 0.0f // 0.01 // Derivative gain

#define PWM_CAP_MIN 14.5 // Minimum capped PWM value
#define PWM_CAP_MAX 17.0 // Maximum capped PWM value

static const pwm1_channel_e motor_pwm = PWM1__2_0;

// Variables for PID control
float integral = 0.0;
float prev_error = 0.0;

static const float motor_pwm_idle_duty_cycle = 15.02; // 0 m/s
static const float motor_pwm_reverse_duty_cycle = 14.50;
static const float motor_pwm_forward_duty_cycle = 15.7; // 0.22 ms

// in meters per sec
static float target_speed;

static float target_duty_cycle;

void motor_speed_pid_set_target_pwm(float target_pwm) {
  float diff = 0.01; // for safe float comparison
  target_duty_cycle = target_pwm;
  if (target_pwm - motor_pwm_forward_duty_cycle < diff) {
    target_speed = 0.22;
  } else if (target_pwm - motor_pwm_idle_duty_cycle < diff) {
    target_speed = 0.0;
  } else if (target_pwm - motor_pwm_reverse_duty_cycle < diff) {
    target_speed = -0.22;
  }
}

float map(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float cap(float value, float min, float max) {
  if (value < min) {
    return min;
  } else if (value > max) {
    return max;
  } else {
    return value;
  }
}

void motor_speed_pid_run_once(void) {

  float current_speed = motor_rpm_driver_get_speed();

  float error = target_speed - current_speed;
  error = 0;

  integral += error;

  float derivative = error - prev_error;

  float output = KP * error + KD * derivative + KI * integral;

  prev_error = error;
  // printf("error: %f ", error);

  output *= 0.45f; // 0.323; // converting from m/s to pwm

  float pwm_value = output + target_duty_cycle;

  // int pwm_value = (int)map(output, -100, 100, PWM_MIN, PWM_MAX);

  pwm_value = cap(pwm_value, PWM_CAP_MIN, PWM_CAP_MAX);

  pwm1__set_duty_cycle(motor_pwm, pwm_value);
  printf("%f,%f\n", (double)target_duty_cycle, (double)pwm_value);

  if (pwm_value < motor_pwm_forward_duty_cycle && pwm_value > motor_pwm_reverse_duty_cycle) {
    gpio__set(board_io__get_led0());
    gpio__reset(board_io__get_led1());
    gpio__set(board_io__get_led2());
  } else if (pwm_value < motor_pwm_idle_duty_cycle) {
    gpio__reset(board_io__get_led0());
    gpio__set(board_io__get_led1());
    gpio__set(board_io__get_led2());
  } else if (pwm_value > motor_pwm_idle_duty_cycle) {
    gpio__set(board_io__get_led0());
    gpio__set(board_io__get_led1());
    gpio__reset(board_io__get_led2());
  }
}