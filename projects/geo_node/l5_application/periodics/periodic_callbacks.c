#include <stdio.h>

#include "board_io.h"
#include "gpio.h"

#include "can_bus_initializer.h"
#include "geo_msg_bank.h"
#include "geo_sensors_compass_lsm.h"
#include "geo_sensors_gps.h"

#include "periodic_callbacks.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_bus_initializer__run_once(can1, 100, 200, 200);

  geo_sensors_gps__init();
  // geo_sensors_compass_lsm__init();
  gpio__set(board_io__get_led0());
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  int gps_lock_status = geo_sensors_gps__get_gps_lock_status();
  // printf("GPS lock status: %d\n", gps_lock_status);
  if (!gps_lock_status) {
    gpio__toggle(board_io__get_led0());
  } else {
    gpio__reset(board_io__get_led0());
  }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {

  geo_msg_bank__handle_msg();
  geo_msg_bank__service_mia_10hz();
  geo_msg_bank__transmit_msg();

  geo_sensors_gps__run_once();
  // geo_sensors_compass_lsm__run_once();
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led2());
  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}
