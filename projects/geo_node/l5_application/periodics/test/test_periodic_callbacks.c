#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

#include "Mockcan_bus_initializer.h"
#include "Mockgeo_msg_bank.h"
#include "Mockgeo_sensors_compass_lsm.h"
#include "Mockgeo_sensors_gps.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__run_once_ExpectAnyArgsAndReturn(1);

  geo_sensors_gps__init_ExpectAndReturn(1);
  // geo_sensors_compass_lsm__init_Expect();

  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz_GPS_lock_not_found(void) {

  geo_sensors_gps__get_gps_lock_status_ExpectAndReturn(0);

  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__1Hz_GPS_lock_found(void) {
  geo_sensors_gps__get_gps_lock_status_ExpectAndReturn(1);

  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  geo_msg_bank__handle_msg_Expect();
  geo_msg_bank__service_mia_10hz_Expect();
  geo_msg_bank__transmit_msg_Expect();

  geo_sensors_gps__run_once_ExpectAndReturn(1);
  // geo_sensors_compass_lsm__run_once_ExpectAndThrow(1);

  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  gpio_s gpio = {};
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  periodic_callbacks__100Hz(0);
}
