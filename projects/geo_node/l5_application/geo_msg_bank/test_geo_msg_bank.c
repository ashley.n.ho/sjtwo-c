#include "unity.h"

#include <stdio.h>

#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"

#include "Mockgeo_logic.h"
#include "Mockgeo_sensors_compass_lsm.h"
#include "Mockgeo_sensors_gps.h"

#include "project.h"

// Include the source we wish to test
#include "geo_msg_bank.c"

void setUp(void) {}

static dbc_GEO_STATUS_s get_stub(void) {
  dbc_GEO_STATUS_s geo_status;
  return geo_status;
}

void test_transmit_message(void) {
  geo_logic__get_distance_to_destination_IgnoreAndReturn(1);
  geo_logic__get_bearing_IgnoreAndReturn(1);
  // geo_sensors_compass_lsm__get_heading_IgnoreAndReturn(1);
  geo_msg_bank__get_heading();

  can__tx_ExpectAnyArgsAndReturn(1);

  gps_coordinates_t dummy_gps = {};
  dummy_gps.latitude = 0;
  dummy_gps.longitude = 0;
  geo_sensors_gps__get_coordinates_IgnoreAndReturn(dummy_gps);
  geo_sensors_gps__get_gps_lock_status_IgnoreAndReturn(1);

  can__tx_ExpectAnyArgsAndReturn(1);

  geo_msg_bank__transmit_msg();
}

void test_msg_bank_gps_destination_location(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_GPS_DESTINATION_LOCATION.message_id};
  dbc_message_header_t header;

  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  can__rx_ExpectAnyArgsAndReturn(0);

  geo_msg_bank__handle_msg();
}

void test_can_handler__manage_mia_10hz(void) {
  dbc_GPS_DESTINATION_LOCATION_s gps_destination_location_test;
  geo_msg_bank__service_mia_10hz();
}
