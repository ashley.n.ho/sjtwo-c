#include <stdio.h>

#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"

#include "geo_logic.h"
#include "geo_msg_bank.h"
//#include "geo_sensors_compass_lsm.h"

#include "geo_sensors_gps.h"

const uint32_t dbc_mia_threshold_GPS_DESTINATION_LOCATION = 1000000000;
const dbc_GPS_DESTINATION_LOCATION_s dbc_mia_replacement_GPS_DESTINATION_LOCATION = {
    .GPS_DEST_LATITUDE_SCALED_100000 = 100000, .GPS_DEST_LONGITUDE_SCALED_100000 = 100000};

static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location = {};
static dbc_GEO_STATUS_s geo_status = {};
static dbc_GPS_HEADING_s geo_gps_heading = {};
static dbc_DBG_MSG_GPS_s dbg_msg_gps = {};

static const float SCALING_FACTOR = 100000.0;

static dbc_GEO_STATUS_s set_geo_status = {
    .GEO_STATUS_DISTANCE_TO_DESTINATION = 0.0, .GEO_STATUS_COMPASS_BEARING = 0.0, .GEO_STATUS_COMPASS_HEADING = 0.0};

/******************** RECIEVING ON CAN ********************/

void geo_msg_bank__handle_msg(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    // Construct "message header" that we need for the decode_*() API
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    geo_msg_bank__receive_msg_GPS_DESTINATION_LOCATION(header, can_msg.data.bytes);
    geo_msg_bank__receive_msg_GPS_HEADING(header, can_msg.data.bytes);
  }
}

void geo_msg_bank__receive_msg_GPS_DESTINATION_LOCATION(dbc_message_header_t header, const uint8_t bytes[8]) {

  dbc_decode_GPS_DESTINATION_LOCATION(&gps_destination_location, header, bytes);
  // printf("Destination latitude: %f, longitude: %f\n",
  // (double)gps_destination_location.GPS_DEST_LATITUDE_SCALED_100000,
  //(double)gps_destination_location.GPS_DEST_LONGITUDE_SCALED_100000);
}

void geo_msg_bank__receive_msg_GPS_HEADING(dbc_message_header_t header, const uint8_t bytes[8]) {

  dbc_decode_GPS_HEADING(&geo_gps_heading, header, bytes);
  // printf("GPS heading recieved: %d", geo_gps_heading.GPS_HEADING_RAW_DEGREES);
}

gps_coordinates_t geo_msg_bank__get_gps_destination_location(void) {
  gps_coordinates_t destination_gps = {};
  destination_gps.latitude = gps_destination_location.GPS_DEST_LATITUDE_SCALED_100000 / SCALING_FACTOR;
  destination_gps.longitude = gps_destination_location.GPS_DEST_LONGITUDE_SCALED_100000 / SCALING_FACTOR;
  // printf("lat:%f long: %f\n", (double)destination_gps.latitude, (double)destination_gps.longitude);
  return destination_gps;
}

uint8_t geo_msg_bank__get_heading(void) {
  uint8_t scaled_heading_degrees = (uint8_t)((geo_gps_heading.GPS_HEADING_RAW_DEGREES) / 20);
  return scaled_heading_degrees;
}

/******************** TRANSMITTING ON CAN ********************/

void geo_msg_bank__transmit_msg(void) {

  geo_status = geo_msg_bank__set_geo_status();
  dbc_encode_and_send_GEO_STATUS(NULL, &geo_status);

  dbg_msg_gps = geo_msg_bank__set_DBG_MSG_GPS();
  dbc_encode_and_send_DBG_MSG_GPS(NULL, &dbg_msg_gps);
}

dbc_GEO_STATUS_s geo_msg_bank__set_geo_status(void) {
  set_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = geo_logic__get_distance_to_destination();
  set_geo_status.GEO_STATUS_COMPASS_BEARING = geo_logic__get_bearing();
  set_geo_status.GEO_STATUS_COMPASS_HEADING = geo_msg_bank__get_heading();
  // printf("GEO_STATUS_DISTANCE_TO_DESTINATION: %f\n", (double)set_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  // printf("GEO_STATUS_COMPASS_BEARING: %f\n", (double)set_geo_status.GEO_STATUS_COMPASS_BEARING);
  // printf("GEO_STATUS_COMPASS_HEADING: %f\n", (double)set_geo_status.GEO_STATUS_COMPASS_HEADING);

  geo_status = set_geo_status;
  return geo_status;
}

dbc_DBG_MSG_GPS_s geo_msg_bank__set_DBG_MSG_GPS(void) {

  gps_coordinates_t current_gps = geo_sensors_gps__get_coordinates();
  float current_latitude = current_gps.latitude;
  float current_longitude = current_gps.longitude;

  dbc_DBG_MSG_GPS_s dbg_msg_gps_local = {};
  dbg_msg_gps_local.DBG_GPS_latitude = current_latitude;
  dbg_msg_gps_local.DBG_GPS_longitude = current_longitude;
  dbg_msg_gps_local.DBG_GPS_lock = geo_sensors_gps__get_gps_lock_status();

  dbg_msg_gps = dbg_msg_gps_local;
  return dbg_msg_gps;
}

/******************** MIA management ********************/
void geo_msg_bank__service_mia_10hz(void) {
  const uint32_t mia_increment_value = 1;

  if (dbc_service_mia_GPS_DESTINATION_LOCATION(&gps_destination_location, mia_increment_value)) {
    gpio__set(board_io__get_led0());
  }
}

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  can__msg_t can_msg = {};

  can_msg.msg_id = message_id;
  can_msg.frame_fields.data_len = dlc;
  memcpy(can_msg.data.bytes, bytes, sizeof(can_msg.data.bytes));

  return (can__tx(can1, &can_msg, 0));
}
