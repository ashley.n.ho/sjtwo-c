#pragma once

#include "geo_sensors_gps.h"
#include "project.h"

gps_coordinates_t geo_msg_bank__get_gps_destination_location(void);
uint8_t geo_msg_bank__get_heading(void);

dbc_GEO_STATUS_s geo_msg_bank__set_geo_status(void);
dbc_DBG_MSG_GPS_s geo_msg_bank__set_DBG_MSG_GPS(void);

void geo_msg_bank__handle_msg(void);
void geo_msg_bank__transmit_msg(void);

void geo_msg_bank__receive_msg_GPS_DESTINATION_LOCATION(dbc_message_header_t header, const uint8_t bytes[8]);
void geo_msg_bank__receive_msg_GPS_HEADING(dbc_message_header_t header, const uint8_t bytes[8]);

void geo_msg_bank__service_mia_10hz(void);
bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc);
