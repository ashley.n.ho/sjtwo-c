

#include "unity.h"

#include <math.h>
#include <stdint.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mocki2c.h"

#include "geo_sensors_compass_lsm.c"

void setUp(void) {}

void tearDown(void) {}

/*
void test_geo_sensors_compass_lsm__init(void) {
  clock__get_peripheral_clock_hz_ExpectAndReturn(1);
  i2c__initialize_Ignore();

  i2c__write_single_ExpectAndReturn(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG1, 0x00, 1);
  i2c__write_single_ExpectAndReturn(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG2, 0x00, 1);
  i2c__write_single_ExpectAndReturn(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG3, 0x00, 1);

  geo_sensors_compass_lsm__init();
}

void test_geo_sensors_compass__read(void) {
  i2c__read_slave_data_IgnoreAndReturn(1);
  float mag[3];
  geo_sensors_compass__calculate_heading(mag);

  geo_sensors_compass__read();
}

void test_geo_sensors_compass_lsm__run_once(void) {
  i2c__read_slave_data_IgnoreAndReturn(1);

  float mag[3];
  geo_sensors_compass__calculate_heading(mag);
  geo_sensors_compass__read();
  geo_sensors_compass_lsm__run_once();
}

void test_geo_sensors_compass__calculate_heading_with_zero_magnitude(void) {
  float mag[3] = {0.0, 0.0, 0.0};
  geo_sensors_compass__calculate_heading(mag);
  TEST_ASSERT_EQUAL_FLOAT(0.0, current_compass_heading);
}

void test_geo_sensors_compass__calculate_heading_normal_case(void) {
  float mag[3] = {1.0, 1.0, 1.0};
  geo_sensors_compass__calculate_heading(mag);
  float expected_heading = 2.0;
  TEST_ASSERT_FLOAT_WITHIN(1.0, expected_heading, current_compass_heading);
}

void test_geo_sensors_compass__calculate_heading_negative_values(void) {
  float mag[3] = {-1.0, -1.0, 0.0};
  geo_sensors_compass__calculate_heading(mag);
  float expected_heading = 11.00;
  TEST_ASSERT_FLOAT_WITHIN(1.0, expected_heading, current_compass_heading);
}


*/