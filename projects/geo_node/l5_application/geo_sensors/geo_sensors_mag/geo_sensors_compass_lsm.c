
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "clock.h"
#include "gpio.h"
#include "i2c.h"

#include "geo_sensors_compass_lsm.h"

static const uint8_t LSM303AGR_MAG_CTRL_REG1 = 0x60;
static const uint8_t LSM303AGR_MAG_CTRL_REG2 = 0x61;
static const uint8_t LSM303AGR_MAG_CTRL_REG3 = 0x62;
static const uint8_t LSM303AGR_MAG_OUTX_L_REG = 0x68;
static const uint8_t magnetometer_read = 0x3D;
static const uint8_t magnetometer_write = 0x3C;

static const i2c_e current_i2c = I2C__2;
const uint32_t i2c_speed_hz = UINT32_C(400) * 1000;
static StaticSemaphore_t binary_semaphore_struct;
static StaticSemaphore_t mutex_struct;
static float current_compass_heading;

#define PI 3.14159265358979323846

void geo_sensors_compass_lsm__init(void) {

  i2c__initialize(current_i2c, i2c_speed_hz, clock__get_peripheral_clock_hz(), &binary_semaphore_struct, &mutex_struct);

  uint8_t ctrl_reg1_value = 0x00;
  uint8_t ctrl_reg2_value = 0x00;
  uint8_t ctrl_reg3_value = 0x00;

  i2c__write_single(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG1, ctrl_reg1_value);
  i2c__write_single(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG2, ctrl_reg2_value);
  i2c__write_single(current_i2c, magnetometer_write, LSM303AGR_MAG_CTRL_REG3, ctrl_reg3_value);
}

static void geo_sensors_compass__calculate_heading(float mag[3]) {
  float magnitude = sqrtf(mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2]);

  const float epsilon = 1e-6;
  if (magnitude < epsilon) {
    current_compass_heading = 0;

  } else {
    float mag_xy = mag[1] / magnitude;
    float mag_xx = mag[0] / magnitude;
    // printf("X: %f, Y: %f\n", (double)mag_xx, (double)mag_xy);
    current_compass_heading = (atan2(mag_xy, mag_xx)) * 180.0 / PI;

    if (current_compass_heading < 0) {
      current_compass_heading = 360 + current_compass_heading;
    }
  }
}

void geo_sensors_compass__read(void) {
  uint8_t buffer[6];
  float mag[3];

  i2c__read_slave_data(current_i2c, magnetometer_read, LSM303AGR_MAG_OUTX_L_REG, buffer, 6);

  mag[0] = (int16_t)(buffer[1] << 8 | buffer[0]);
  mag[1] = (int16_t)(buffer[3] << 8 | buffer[2]);
  mag[2] = (int16_t)(buffer[5] << 8 | buffer[4]);

  geo_sensors_compass__calculate_heading(mag);
}

float geo_sensors_compass_lsm__get_heading(void) { return current_compass_heading; }

void geo_sensors_compass_lsm__run_once(void) { geo_sensors_compass__read(); }
