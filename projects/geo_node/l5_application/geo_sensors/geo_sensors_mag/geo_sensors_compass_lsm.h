

#pragma once

void geo_sensors_compass_lsm__init(void);
void geo_sensors_compass__read(void);
float geo_sensors_compass_lsm__get_heading(void);
void geo_sensors_compass_lsm__run_once(void);
