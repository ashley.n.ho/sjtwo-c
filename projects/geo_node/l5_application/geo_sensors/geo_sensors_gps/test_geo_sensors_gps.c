
#include "unity.h"

#include <stdbool.h>
#include <stdlib.h>

#include "Mockboard_io.h"
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"

#include "line_buffer.h"

#include "geo_sensors_gps.c"

void setUp(void) {
  memset(&parsed_coordinates, 0, sizeof(parsed_coordinates));
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  memset(&line_buffer, 0, sizeof(line_buffer));
}

void tearDown(void) {
  memset(&parsed_coordinates, 0, sizeof(parsed_coordinates));
  memset(&line_buffer, 0, sizeof(line_buffer));
}

void test_geo_sensors_gps__init(void) {

  gpio_s dummy_gpio_instance_rx;
  gpio__port_e port = gps_port;
  dummy_gpio_instance_rx.port_number = port;
  dummy_gpio_instance_rx.pin_number = rx_pin;

  gpio_s dummy_gpio_instance_tx;
  dummy_gpio_instance_tx.port_number = port;
  dummy_gpio_instance_tx.pin_number = tx_pin;

  gpio__construct_with_function_ExpectAndReturn(gps_port, tx_pin, GPIO__FUNCTION_2, dummy_gpio_instance_tx);
  gpio__construct_with_function_ExpectAndReturn(gps_port, rx_pin, GPIO__FUNCTION_2, dummy_gpio_instance_rx);

  clock__get_peripheral_clock_hz_ExpectAndReturn(true);
  uart__init_Expect(uart_gps, true, gps_baud_rate);
  uart__is_initialized_ExpectAndReturn(uart_gps, true);

  QueueHandle_t rx, tx;
  xQueueCreate_ExpectAndReturn(rx_queue_size, sizeof(char), rx);
  xQueueCreate_ExpectAndReturn(tx_queue_size, sizeof(char), tx);
  uart__enable_queues_ExpectAndReturn(uart_gps, rx, tx, true);

  uart__is_transmit_queue_initialized_ExpectAndReturn(uart_gps, true);
  uart__is_receive_queue_initialized_ExpectAndReturn(uart_gps, true);

  geo_sensors_gps__init();
}

void test_geo_sensors_gps__init_gpio(void) {

  gpio_s dummy_gpio_instance_rx;
  gpio__port_e port = gps_port;
  dummy_gpio_instance_rx.port_number = port;
  dummy_gpio_instance_rx.pin_number = rx_pin;

  gpio_s dummy_gpio_instance_tx;
  dummy_gpio_instance_tx.port_number = port;
  dummy_gpio_instance_tx.pin_number = tx_pin;

  gpio__construct_with_function_ExpectAndReturn(gps_port, tx_pin, GPIO__FUNCTION_2, dummy_gpio_instance_tx);
  gpio__construct_with_function_ExpectAndReturn(gps_port, rx_pin, GPIO__FUNCTION_2, dummy_gpio_instance_rx);

  geo_sensors_gps__init_gpio();
}

void test_geo_sensors_gps__init_uart(void) {
  clock__get_peripheral_clock_hz_ExpectAndReturn(true);
  uart__init_Expect(uart_gps, true, gps_baud_rate);
  uart__is_initialized_ExpectAndReturn(UART__3, true);

  geo_sensors_gps__init_uart();
}

void test_geo_sensors_gps__init_queue(void) {
  QueueHandle_t rx, tx;
  xQueueCreate_ExpectAndReturn(rx_queue_size, sizeof(char), rx);
  xQueueCreate_ExpectAndReturn(tx_queue_size, sizeof(char), tx);
  uart__enable_queues_ExpectAndReturn(uart_gps, rx, tx, true);

  uart__is_transmit_queue_initialized_ExpectAndReturn(uart_gps, true);
  uart__is_receive_queue_initialized_ExpectAndReturn(uart_gps, true);

  geo_sensors_gps__init_queue();
}

void test_GPGLL_line_is_ignored(void) {
  const char *uart_driver_returned_data = "$GPGLL,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";

  uart__get_IgnoreAndReturn(false);
  parsed_coordinates = geo_sensors_gps__get_coordinates();

  geo_sensors_gps__run_once();
  TEST_ASSERT_EQUAL_FLOAT(0.0, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL_FLOAT(0.0, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed(void) {
  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,1122.33,N,4455.66,E,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];
    const bool last_char = (index < strlen(uart_driver_returned_data));

    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  geo_sensors_gps__run_once();

  parsed_coordinates = geo_sensors_gps__get_coordinates();
  TEST_ASSERT_EQUAL(-31.961166, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL(-83.405663, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_latitude_negative(void) {
  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,1122.33,S,4455.66,E,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];
    const bool last_char = (index < strlen(uart_driver_returned_data));

    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  geo_sensors_gps__run_once();

  parsed_coordinates = geo_sensors_gps__get_coordinates();
  TEST_ASSERT_EQUAL(31.961166, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL(-83.405663, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_longitude_negative(void) {
  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,1122.33,N,4455.66,W,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];
    const bool last_char = (index < strlen(uart_driver_returned_data));

    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  geo_sensors_gps__run_once();

  parsed_coordinates = geo_sensors_gps__get_coordinates();
  TEST_ASSERT_EQUAL(-31.961166, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL(83.405663, parsed_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_both_negative(void) {
  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,1122.33,S,4455.66,W,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];
    const bool last_char = (index < strlen(uart_driver_returned_data));

    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  geo_sensors_gps__run_once();

  parsed_coordinates = geo_sensors_gps__get_coordinates();
  TEST_ASSERT_EQUAL(31.961166, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL(83.405663, parsed_coordinates.longitude);
}

void test_GPGGA_incomplete_line(void) {
  const char *uart_driver_returned_data = "$GPGGA,hhmmss.ss,1122.33,S,4455.66,W,x,";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];
    const bool last_char = (index < strlen(uart_driver_returned_data));

    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  geo_sensors_gps__run_once();

  parsed_coordinates = geo_sensors_gps__get_coordinates();
  TEST_ASSERT_EQUAL(0.0, parsed_coordinates.latitude);
  TEST_ASSERT_EQUAL(0.0, parsed_coordinates.longitude);
}