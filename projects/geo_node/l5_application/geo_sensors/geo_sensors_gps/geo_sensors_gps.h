#pragma once

#include <stdbool.h>

typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

bool geo_sensors_gps__init(void);
bool geo_sensors_gps__init_gpio(void);
bool geo_sensors_gps__init_uart(void);
bool geo_sensors_gps__init_queue(void);

bool geo_sensors_gps__transfer_data_from_uart_driver_to_line_buffer(void);
bool geo_sensors_gps__run_once(void);
int geo_sensors_gps__get_gps_lock_status(void);

gps_coordinates_t geo_sensors_gps__get_coordinates(void);
