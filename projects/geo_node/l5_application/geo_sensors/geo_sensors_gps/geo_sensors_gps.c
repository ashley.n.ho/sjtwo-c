
#include <stdio.h>
#include <stdlib.h>

#include "board_io.h"
#include "clock.h"
#include "gpio.h"
#include "uart.h"

#include "geo_sensors_gps.h"

#include "line_buffer.h"

static const uart_e uart_gps = UART__3;
static const gpio__port_e gps_port = GPIO__PORT_4;
static const uint8_t rx_pin = 29;
static const uint8_t tx_pin = 28;
static const int gps_baud_rate = 9600;
static const int rx_queue_size = 1000;
static const int tx_queue_size = 1000;

static char line_buffer[1000];
static line_buffer_s line;
static gps_coordinates_t parsed_coordinates;

static int gps_quality = 0;

bool geo_sensors_gps__init(void) {
  bool gps_initialized = true;

  bool gpio_status = geo_sensors_gps__init_gpio();
  bool uart_status = geo_sensors_gps__init_uart();
  bool queue_status = geo_sensors_gps__init_queue();

  gps_initialized = gpio_status && uart_status && queue_status;

  return gps_initialized;
}

bool geo_sensors_gps__init_gpio(void) {
  bool status_success = true;

  gpio__construct_with_function(GPIO__PORT_4, tx_pin, GPIO__FUNCTION_2); // P4.28 as TXD3
  gpio__construct_with_function(GPIO__PORT_4, rx_pin, GPIO__FUNCTION_2); // P4.29 as RXD3

  return status_success;
}

bool geo_sensors_gps__init_uart(void) {
  bool status_success = true;

  // Line buffer init
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  // UART init
  uart__init(uart_gps, clock__get_peripheral_clock_hz(), gps_baud_rate);
  if (!uart__is_initialized(uart_gps)) {
    return !status_success;
  }

  return status_success;
}

bool geo_sensors_gps__init_queue(void) {
  bool status_success = true;

  // Queue init
  QueueHandle_t rxq_handle = xQueueCreate(rx_queue_size, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(tx_queue_size, sizeof(char));
  uart__enable_queues(uart_gps, rxq_handle, txq_handle);

  if (!uart__is_transmit_queue_initialized(uart_gps) || !(uart__is_receive_queue_initialized(uart_gps))) {
    return !status_success;
  }

  return status_success;
}

bool geo_sensors_gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte_read;
  const uint32_t zero_timeout = 0;
  bool gps_status_reading = false;

  while (uart__get(uart_gps, &byte_read, zero_timeout)) {
    // printf("GPS: %s\n", &byte_read);
    gps_status_reading = true;
    line_buffer__add_byte(&line, byte_read);
  }

  return gps_status_reading;
}

int geo_sensors_gps__get_gps_lock_status(void) { return gps_quality; }

gps_coordinates_t geo_sensors_gps__get_coordinates(void) { return parsed_coordinates; }

static void gps__parse_coordinates_from_line(void) {
  char gps_line[1000];
  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {
    // Check if the line is a GPGGA stream
    if (strncmp(gps_line, "$GPGGA,", 7) == 0) {
      char *token;
      int field_idx = 0;
      int latitude_flag = 1;
      int longitude_flag = 1;
      float latitude = 0.0;
      float longitude = 0.0;

      token = strtok(gps_line, ",");
      while (token != NULL) {
        token = strtok(NULL, ",");
        field_idx++;

        if (field_idx == 2) { // Latitude is field 2 in GPGGA
          latitude = atof(token);
        } else if (field_idx == 3) { // N/S direction is field 3 in GPGGA
          char c = token[0];
          if (c == 'S') {
            latitude_flag *= -1;
          }
        } else if (field_idx == 4) { // Longitude is field 4 in GPGGA
          longitude = atof(token);
        } else if (field_idx == 5) { // E/W direction is field 5 in GPGGA
          char c = token[0];
          if (c == 'W') {
            longitude_flag *= -1;
          }
        } else if (field_idx == 6) {
          char c = token[0];
          // printf("GPS QUALITY: %c\n", c);
          if (c == '0') {
            gps_quality = 0;
          } else if (c == '1') {
            gps_quality = 1;
            // printf("GPS lock successful\n");
          } else {
            // printf("Trying to acquire GPS fix\n");
          }
        } else if (field_idx > 6) {
          break;
        }
      }

      int latitude_days = latitude / 100;
      int longitude_days = longitude / 100;
      float latitude_minutes = latitude - 3700.00;
      float longitude_minutes = longitude - 12100.00;

      latitude_minutes /= 60;
      longitude_minutes /= 60;

      parsed_coordinates.latitude = ((1.0 * latitude_days) + latitude_minutes) * latitude_flag;
      parsed_coordinates.longitude = ((1.0 * longitude_days) + longitude_minutes) * longitude_flag;
    }
    // printf("Parsed Latitude: %f, Longitude: %f\n", (double)parsed_coordinates.latitude,
    //       (double)parsed_coordinates.longitude);
  }
}

bool geo_sensors_gps__run_once(void) {
  bool gps_transfer_success = false;

  gps_transfer_success = geo_sensors_gps__transfer_data_from_uart_driver_to_line_buffer();
  gps__parse_coordinates_from_line();

  return gps_transfer_success;
}
