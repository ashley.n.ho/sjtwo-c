#pragma once

#include "geo_sensors_gps.h"

gps_coordinates_t find_next_point(gps_coordinates_t origin, gps_coordinates_t destination);
