#include "unity.h"

#include <math.h>

#include "Mockgeo_sensors_gps.h"
#include "project.h"

// Include the source we wish to test
#include "check_point_algo.h"

void setUp(void) {}

void test_find_next_point() {
  gps_coordinates_t origin = {0, 0};
  gps_coordinates_t destination = {60, 150};

  gps_coordinates_t expected_result[] = {
      {20, 10},
      {50, 30},
      {30, 90},
      {60, 150},
  };

  for (int i = 0; i < 4; i++) {

    gps_coordinates_t next_point = find_next_point(origin, destination);

    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].latitude, next_point.latitude);
    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].longitude, next_point.longitude);

    origin = next_point;
  }
}

void test_find_next_point_when_reach_destination_early() {
  gps_coordinates_t origin = {0, 0};
  gps_coordinates_t destination = {60, 150};

  gps_coordinates_t expected_result[] = {
      {20, 10}, {50, 30}, {30, 90}, {60, 150}, {60, 150}, // reach destination index 3
  };

  for (int i = 0; i < 5; i++) {

    gps_coordinates_t next_point = find_next_point(origin, destination);

    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].latitude, next_point.latitude);
    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].longitude, next_point.longitude);

    origin = next_point;
  }
}

void test_find_next_point_when_already_at_destination() {
  gps_coordinates_t origin = {60, 150};
  gps_coordinates_t destination = {60, 150};

  gps_coordinates_t expected_result[] = {
      {60, 150}, {60, 150}, {60, 150}, {60, 150}, {60, 150}, // should alwasy return current location
  };

  for (int i = 0; i < 5; i++) {

    gps_coordinates_t next_point = find_next_point(origin, destination);

    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].latitude, next_point.latitude);
    TEST_ASSERT_EQUAL_FLOAT(expected_result[i].longitude, next_point.longitude);
  }
}