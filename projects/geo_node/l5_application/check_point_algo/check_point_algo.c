#include "check_point_algo.h"
#include "math.h"
#include <float.h>
#include <stdio.h>

#define NUM_OF_CHECKPOINTS 4

const int CP_RADIUS_EARTH_KM = 6371.0;
const float CP_PI_VAL = 3.1415926;

/* unit test values
    {50, 30},
    {20, 10},
    {30, 90},
    {35, -45},
  */

static const gps_coordinates_t locations_we_can_travel[NUM_OF_CHECKPOINTS] = {
    //(la, lo)
    // example values
    {50, 30},
    {20, 10},
    {30, 90},
    {35, -45},
    /*{37.339084, -121.880836},
    {37.339178, -121.880861},
    {37.339318, -121.880970},
    {37.339382, -121.881054},*/
};

static float get_distance_between_points(gps_coordinates_t p1, gps_coordinates_t p2) {
  /*float distance = sqrt((p1.latitude - p2.latitude) * (p1.latitude - p2.latitude) +
                        (p1.longitude - p2.longitude) * (p1.longitude - p2.longitude));*/
  float current_latitude = p1.latitude;
  float current_longitude = p1.longitude;

  float destination_latitude = p2.latitude;
  float destination_longitude = p2.longitude;

  float phi1 = (float)(current_latitude * (float)CP_PI_VAL / (float)180.0);
  float phi2 = (float)(destination_latitude * CP_PI_VAL / (float)180.0);
  float delta_phi = (float)(destination_latitude - current_latitude) * CP_PI_VAL / (float)180.0;
  float delta_lambda = (float)(destination_longitude - current_longitude) * CP_PI_VAL / (float)180.0;

  float a =
      sin(delta_phi / 2) * sin(delta_phi / 2) + cos(phi1) * cos(phi2) * sin(delta_lambda / 2) * sin(delta_lambda / 2);
  float c = 2 * atan2(sqrt(a), sqrt(1 - a));
  float distance_to_destination = CP_RADIUS_EARTH_KM * c * 1000;

  return distance_to_destination;
}

gps_coordinates_t find_next_point(gps_coordinates_t origin, gps_coordinates_t destination) {
  gps_coordinates_t closest_point;
  float min_distance = FLT_MAX;
  float distance;
  int checkpoint_idx = -1;

  for (int i = 0; i < NUM_OF_CHECKPOINTS; i++) {
    distance = get_distance_between_points(origin, locations_we_can_travel[i]);
    if (distance < min_distance) {
      if (get_distance_between_points(origin, locations_we_can_travel[i]) < 5) {
        printf("reached checkpoint\n");
        continue;
      }
      // Check if point is actually closer to destincation
      if (get_distance_between_points(locations_we_can_travel[i], destination) <
          get_distance_between_points(origin, destination)) {
        min_distance = distance;
        checkpoint_idx = i;
        printf("setting checkpoint\n");
      } else {
        continue;
      }
    }
  }
  if (checkpoint_idx < 0) {
    closest_point = destination;
    printf("Going destination\n");
  } else {
    printf("setting closest_point\n");
    closest_point = locations_we_can_travel[checkpoint_idx];
  }
  printf("p: %f, %f", closest_point.latitude, closest_point.longitude);
  return closest_point;
}
