#include "line_buffer.h"

/************* PART 1 *************/

void line_buffer__init(line_buffer_s *buffer, void *memory, size_t size) {
  buffer->memory = memory;
  buffer->max_size = size;
  buffer->write_index = 0;
}

bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool byte_added = false;
  if (buffer->write_index < buffer->max_size) {
    char *mem_buffer = (char *)buffer->memory;

    mem_buffer[buffer->write_index] = byte;
    buffer->write_index++;
    byte_added = true;
  }

  return byte_added;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  if (line_max_size == 0) {
    return false;
  }

  char *mem = (char *)buffer->memory;
  bool line_removed = false;

  for (size_t i = 0; i < buffer->write_index; ++i) {
    if (mem[i] == '\n') {
      if (i >= line_max_size) {
        i = line_max_size - 1;
      }

      memcpy(line, mem, i); // copy buffer memory into line
      line[i] = '\0';

      memmove(mem, mem + i + 1, buffer->write_index - (i + 1)); // shift buffer data over after removing line
      buffer->write_index -= (i + 1);

      line_removed = true;
      break;
    }
  }

  // Accounting for edge case if buffer is full
  if (!line_removed && (buffer->write_index == buffer->max_size)) {
    size_t i = line_max_size - 1 < buffer->write_index ? line_max_size - 1 : buffer->write_index;
    memcpy(line, mem, i);
    line[i] = '\0';
    buffer->write_index = 0;

    line_removed = true;
  }

  return line_removed;
}