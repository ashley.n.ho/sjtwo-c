#include "unity.h"

#include "Mockboard_io.h"
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"

#include "bluetooth_module.c"

void setUp(void) {}

void tearDown(void) {}

void test_geo_sensors_bluetooth_module__init(void) {

  gpio_s dummy_gpio_instance_input;
  gpio__port_e port = bt_port;
  dummy_gpio_instance_input.port_number = bt_port;
  dummy_gpio_instance_input.pin_number = rx_pin;

  gpio_s dummy_gpio_instance_output;
  dummy_gpio_instance_output.port_number = port;
  dummy_gpio_instance_output.pin_number = tx_pin;

  gpio__construct_as_input_ExpectAndReturn(bt_port, rx_pin, dummy_gpio_instance_input);
  gpio__construct_as_output_ExpectAndReturn(bt_port, tx_pin, dummy_gpio_instance_output);

  uart__init_Expect(uart_bluetooth, 96000000, bt_baud_rate);
  uart__is_initialized_ExpectAndReturn(uart_bluetooth, true);

  QueueHandle_t rx, tx;
  xQueueCreate_ExpectAndReturn(rx_queue_size, sizeof(char), rx);
  xQueueCreate_ExpectAndReturn(tx_queue_size, sizeof(char), tx);
  uart__enable_queues_ExpectAndReturn(uart_bluetooth, rx, tx, true);

  uart__is_transmit_queue_initialized_ExpectAndReturn(uart_bluetooth, true);
  uart__is_receive_queue_initialized_ExpectAndReturn(uart_bluetooth, true);

  geo_sensors_bluetooth_module__init();
}

void test_geo_sensors_bluetooth_module__init_gpio(void) {
  gpio_s dummy_gpio_instance_input;
  dummy_gpio_instance_input.port_number = bt_port;
  dummy_gpio_instance_input.pin_number = rx_pin;

  gpio_s dummy_gpio_instance_output;
  dummy_gpio_instance_output.port_number = bt_port;
  dummy_gpio_instance_output.pin_number = tx_pin;

  gpio__construct_as_input_ExpectAndReturn(bt_port, rx_pin, dummy_gpio_instance_input);
  gpio__construct_as_output_ExpectAndReturn(bt_port, tx_pin, dummy_gpio_instance_output);

  geo_sensors_bluetooth_module__init_gpio();
}

void test_geo_sensors_bluetooth_module__init_uart(void) {
  uart__init_Expect(uart_bluetooth, 96000000, bt_baud_rate);
  uart__is_initialized_ExpectAndReturn(uart_bluetooth, true);

  geo_sensors_bluetooth_module__init_uart();
}

void test_geo_sensors_bluetooth_module__init_queue(void) {
  QueueHandle_t rx, tx;
  xQueueCreate_ExpectAndReturn(rx_queue_size, sizeof(char), rx);
  xQueueCreate_ExpectAndReturn(tx_queue_size, sizeof(char), tx);
  uart__enable_queues_ExpectAndReturn(uart_bluetooth, rx, tx, true);

  uart__is_transmit_queue_initialized_ExpectAndReturn(uart_bluetooth, true);
  uart__is_receive_queue_initialized_ExpectAndReturn(uart_bluetooth, true);

  geo_sensors_bluetooth_module__init_queue();
}
