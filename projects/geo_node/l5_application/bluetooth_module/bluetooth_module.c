#include "bluetooth_module.h"

static const uart_e uart_bluetooth = UART__1;
static const gpio__port_e bt_port = GPIO__PORT_1;
static const uint8_t rx_pin = 4;
static const uint8_t tx_pin = 5;
static const int bt_baud_rate = 115200;
static const int rx_queue_size = 50;
static const int tx_queue_size = 8;

bool geo_sensors_bluetooth_module__init(void) {
  bool bluetooth_initialized = false;

  bool gpio_status = geo_sensors_bluetooth_module__init_gpio();
  bool uart_status = geo_sensors_bluetooth_module__init_uart();
  bool queue_status = geo_sensors_bluetooth_module__init_queue();

  bluetooth_initialized = gpio_status && uart_status && queue_status;
  return bluetooth_initialized;
}

bool geo_sensors_bluetooth_module__init_gpio(void) {
  bool status_success = true;

  // GPIO ports set
  gpio__construct_as_input(bt_port, rx_pin);  // RX pin
  gpio__construct_as_output(bt_port, tx_pin); // TX pin

  return status_success;
}

bool geo_sensors_bluetooth_module__init_uart(void) {
  bool status_success = true;

  // UART init
  uart__init(uart_bluetooth, 96000000, bt_baud_rate);
  if (!uart__is_initialized(uart_bluetooth)) {
    return !status_success;
  }

  return status_success;
}

bool geo_sensors_bluetooth_module__init_queue(void) {
  bool status_success = true;

  // Queue init
  QueueHandle_t rxq_handle = xQueueCreate(rx_queue_size, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(tx_queue_size, sizeof(char));
  uart__enable_queues(uart_bluetooth, rxq_handle, txq_handle);

  if (!uart__is_transmit_queue_initialized(uart_bluetooth) || !(uart__is_receive_queue_initialized(uart_bluetooth))) {
    return !status_success;
  }

  return status_success;
}

bool geo_sensors_bluetooth_module__run_once(void) {
  bool bluetooth_transfer_success = false;

  bluetooth_transfer_success = geo_sensors_bluetooth_module__get_data();
  return bluetooth_transfer_success;
}

bool geo_sensors_bluetooth_module__get_data(void) {
  char byte_read;
  const uint32_t zero_timeout = 0;
  bool bluetooth_status_reading = false;
  while (uart__get(uart_bluetooth, &byte_read, zero_timeout)) {
    // line_buffer__add_byte(&line, byte_read);
  }

  return bluetooth_status_reading;
}

bool geo_sensors_bluetooth_module__send_data(void) {}
