#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "board_io.h"
#include "clock.h"
#include "gpio.h"
#include "uart.h"

bool geo_sensors_bluetooth_module__init(void);
bool geo_sensors_bluetooth_module__init_gpio(void);
bool geo_sensors_bluetooth_module__init_uart(void);
bool geo_sensors_bluetooth_module__init_queue(void);

bool geo_sensors_bluetooth_module__run_once(void);

bool geo_sensors_bluetooth_module__get_data(void);
bool geo_sensors_bluetooth_module__send_data(void);