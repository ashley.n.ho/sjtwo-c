#pragma once

#include "project.h"

void geo_logic(void);
void geo_logic__haversine_calculation(void);
dbc_GEO_STATUS_s geo_logic__get_geo_status(void);

uint8_t geo_logic__get_bearing(void);

float geo_logic__get_distance_to_destination(void);
