#include "unity.h"

#include <math.h>
#include <stdlib.h>

#include "Mockcheck_point_algo.h"
#include "Mockgeo_msg_bank.h"
#include "Mockgeo_sensors_compass_lsm.h"
#include "Mockgeo_sensors_gps.h"
#include "project.h"

// Include the source we wish to test
#include "geo_logic.c"

void setUp(void) {}

static gps_coordinates_t gps_coord_stub(int callback_count) {
  gps_coordinates_t current_gps;
  current_gps.latitude = 1.0;
  current_gps.longitude = 2.0;
  return current_gps;
}

void test_geo_logic(void) {
  gps_coordinates_t destination_gps;
  destination_gps.latitude = 37.0;
  destination_gps.longitude = -121.0;
  geo_msg_bank__get_gps_destination_location_ExpectAndReturn(destination_gps);
  geo_sensors_gps__get_coordinates_Stub(&gps_coord_stub);
  find_next_point_ExpectAnyArgsAndReturn(destination_gps);
  geo_msg_bank__get_heading_ExpectAndReturn(0);

  // geo_sensors_compass_lsm__get_heading_ExpectAndReturn(1);

  geo_logic();

  TEST_ASSERT_EQUAL(12799937, current_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  TEST_ASSERT_EQUAL(15.00, current_geo_status.GEO_STATUS_COMPASS_BEARING);
  TEST_ASSERT_EQUAL(0, current_geo_status.GEO_STATUS_COMPASS_HEADING);
}

void test_geo_logic_zero_distance(void) {
  gps_coordinates_t destination_gps;
  destination_gps.latitude = 37.0;
  destination_gps.longitude = -121.0;

  geo_msg_bank__get_gps_destination_location_ExpectAndReturn(destination_gps);
  geo_sensors_gps__get_coordinates_Stub(&gps_coord_stub);
  find_next_point_ExpectAnyArgsAndReturn(destination_gps);
  geo_msg_bank__get_heading_ExpectAndReturn(0);
  // geo_sensors_compass_lsm__get_heading_ExpectAndReturn(10);

  geo_logic();

  TEST_ASSERT_EQUAL_FLOAT(12799940, current_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  TEST_ASSERT_TRUE(isnan(current_geo_status.GEO_STATUS_COMPASS_BEARING) ||
                   current_geo_status.GEO_STATUS_COMPASS_BEARING != 0);
  TEST_ASSERT_EQUAL(0, current_geo_status.GEO_STATUS_COMPASS_HEADING);
}

void test_geo_logic_max_distance(void) {
  gps_coordinates_t destination_gps;
  destination_gps.latitude = 37.0;
  destination_gps.longitude = -121.0;

  geo_msg_bank__get_gps_destination_location_ExpectAndReturn(destination_gps);
  geo_sensors_gps__get_coordinates_Stub(&gps_coord_stub);
  find_next_point_ExpectAnyArgsAndReturn(destination_gps);
  geo_msg_bank__get_heading_ExpectAndReturn(1);

  // geo_sensors_compass_lsm__get_heading_ExpectAndReturn(30);

  geo_logic();

  TEST_ASSERT_FLOAT_WITHIN(100000.0, 1.279994e+07, current_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  TEST_ASSERT_EQUAL(1, current_geo_status.GEO_STATUS_COMPASS_HEADING);
}

static gps_coordinates_t gps_coord_stub2(int call) {
  gps_coordinates_t current_gps;
  current_gps.latitude = 0.0;
  current_gps.longitude = -1.0;
  return current_gps;
}

void test_geo_logic_prime_meridian_crossing(void) {
  gps_coordinates_t destination_gps = {.latitude = 0.0, .longitude = 1.0};
  geo_msg_bank__get_gps_destination_location_ExpectAndReturn(destination_gps);

  geo_sensors_gps__get_coordinates_Stub(&gps_coord_stub2);
  find_next_point_ExpectAnyArgsAndReturn(destination_gps);
  geo_msg_bank__get_heading_ExpectAndReturn(180);

  // geo_sensors_compass_lsm__get_heading_ExpectAndReturn(180);

  geo_logic();
  // printf("Distance: %f, Bearing: %f\n", distance_to_destination, compass_bearing);
  TEST_ASSERT_FLOAT_WITHIN(100000.0, 222389.576, current_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  TEST_ASSERT_FLOAT_WITHIN(0.01, 90.00, compass_bearing); // Expect near 90 degrees bearing
  TEST_ASSERT_FLOAT_WITHIN(0.01, 4, current_geo_status.GEO_STATUS_COMPASS_BEARING);
}
