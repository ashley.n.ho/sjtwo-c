#include <math.h>
#include <stdio.h>

#include "check_point_algo.h"
#include "geo_logic.h"
#include "geo_msg_bank.h"
#include "geo_sensors_compass_lsm.h"
#include "geo_sensors_gps.h"

static dbc_GEO_STATUS_s current_geo_status = {};

const int RADIUS_EARTH_KM = 6371.0;
const float PI_VAL = 3.1415926;

static float compass_bearing = 0;
static float compass_heading = 0;
static float distance_to_destination = 0.0;

dbc_GEO_STATUS_s geo_logic__get_geo_status(void) {
  geo_logic();
  return current_geo_status;
}

uint8_t geo_logic__get_bearing(void) {
  geo_logic();
  uint8_t compass_bearing_transmit = (uint8_t)(compass_bearing / 20);
  return compass_bearing_transmit;
}

float geo_logic__get_distance_to_destination(void) {
  geo_logic();
  return distance_to_destination;
}

void geo_logic(void) { geo_logic__haversine_calculation(); }

void geo_logic__haversine_calculation(void) {
  // Input: Latitude longitude, Output: heading, bearing, distance to destination
  gps_coordinates_t current_gps = geo_sensors_gps__get_coordinates();
  float current_latitude = current_gps.latitude;
  float current_longitude = current_gps.longitude;
  // printf("Latitude: %f, Longitude: %f\n", (double)current_latitude, (double)current_longitude);

  gps_coordinates_t destination_gps = geo_msg_bank__get_gps_destination_location();
  if (destination_gps.latitude == 0 && destination_gps.longitude == 0) {
    destination_gps.latitude = current_latitude;
    destination_gps.longitude = current_longitude;
  }
  gps_coordinates_t next_point = find_next_point(current_gps, destination_gps);
  float destination_latitude = next_point.latitude;
  float destination_longitude = next_point.longitude;
  // printf("\nDEST: Latitude: %f, Longitude: %f\n", (double)destination_latitude, (double)destination_longitude);
  // Convert latitudes and longitudes from degrees to radians
  float phi1 = (float)(current_latitude * (float)PI_VAL / (float)180.0);
  float phi2 = (float)(destination_latitude * PI_VAL / (float)180.0);
  float delta_phi = (float)(destination_latitude - current_latitude) * PI_VAL / (float)180.0;
  float delta_lambda = (float)(destination_longitude - current_longitude) * PI_VAL / (float)180.0;

  // Haversine formula
  float a =
      sin(delta_phi / 2) * sin(delta_phi / 2) + cos(phi1) * cos(phi2) * sin(delta_lambda / 2) * sin(delta_lambda / 2);
  float c = 2 * atan2(sqrt(a), sqrt(1 - a));
  distance_to_destination = RADIUS_EARTH_KM * c * 1000;

  // Bearing calculation
  float y = sin(delta_lambda) * cos(phi2);
  float x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(delta_lambda);
  float bearing_rad = atan2(y, x);
  compass_bearing = fmod((bearing_rad * (float)180.0 / (float)PI_VAL + (float)360.0), 360.0);

  compass_heading = geo_msg_bank__get_heading();
  // printf("Bearing: %f, Heading: %f \n", (double)compass_bearing, (double)compass_heading);

  current_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = distance_to_destination;
  current_geo_status.GEO_STATUS_COMPASS_BEARING = (uint8_t)(compass_bearing / 20);
  current_geo_status.GEO_STATUS_COMPASS_HEADING = (uint8_t)(compass_heading);
}
