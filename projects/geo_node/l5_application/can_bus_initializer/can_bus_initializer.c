#include "can_bus_initializer.h"
#include "can_bus.h"

#include <stdbool.h>
#include <stddef.h>

bool can_bus_initializer__run_once(can__num_e can, uint32_t baudrate_kbps, uint16_t rxq_size, uint16_t txq_size) {
  bool initialized = false;

  if (can == can1 || can == can2) {
    if (baudrate_kbps == 100 || baudrate_kbps == 250 || baudrate_kbps == 500 || baudrate_kbps == 1000) {
      if (rxq_size != 0 && txq_size != 0) {
        initialized = can__init(can, baudrate_kbps, rxq_size, txq_size, NULL, NULL);
        can__bypass_filter_accept_all_msgs();
        can__reset_bus(can);
      }
    }
  }

  return initialized;
}
