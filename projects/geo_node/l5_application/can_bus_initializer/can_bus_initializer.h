#pragma once

#include "can_bus.h"

bool can_bus_initializer__run_once(can__num_e can, uint32_t baudrate_kbps, uint16_t rxq_size, uint16_t txq_size);