#pragma once

#include "project.h"

dbc_DBG_MSG_MOTOR_s sensor_msg_bank__get_dbg_msg(void);

void sensor_msg_bank__handle_msg(void);