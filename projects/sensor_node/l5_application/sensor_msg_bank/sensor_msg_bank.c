#include "sensor_msg_bank.h"
#include "bridge.h"
#include "can_bus.h"
#include <stdio.h>

static dbc_DBG_MSG_MOTOR_s dbg_msg;
static dbc_GEO_STATUS_s geo_msg;

dbc_DBG_MSG_MOTOR_s sensor_msg_bank__get_dbg_msg(void) { return dbg_msg; }

static void sensor_msg_bank__handle_dbg_msg(dbc_message_header_t header, const uint8_t bytes[8]) {
  if (dbc_decode_DBG_MSG_MOTOR(&dbg_msg, header, bytes)) {
    // printf("received: %f\n", dbg_msg.SPEED_M_PER_S);
    // uint8_t dbg_id = 0x01;
    // uint8_t data[5];
    // uint8_t *data_ptr = &data[1];
    // data[0] = dbg_id;
    // data_ptr = (uint32_t)dbg_msg.SPEED_M_PER_S;

    bridge_send_data((void *)&dbg_msg.SPEED_M_PER_S, 4, 0);
  }
}

static void sensor_msg_bank__handle_geo_status_msg(dbc_message_header_t header, const uint8_t bytes[8]) {
  if (dbc_decode_GEO_STATUS(&geo_msg, header, bytes)) {
    // printf("received: %f\n", dbg_msg.SPEED_M_PER_S);
    // uint8_t dbg_id = 0x01;
    // uint8_t data[5];
    // uint8_t *data_ptr = &data[1];
    // data[0] = dbg_id;
    // data_ptr = (uint32_t)dbg_msg.SPEED_M_PER_S;

    bridge_send_data((void *)&geo_msg, 4, 1);
  }
}

void sensor_msg_bank__handle_msg(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    sensor_msg_bank__handle_dbg_msg(header, can_msg.data.bytes);
    sensor_msg_bank__handle_geo_status_msg(header, can_msg.data.bytes);
  }
}

// TODO: update
// MIA management:
/*
void motor_msg_bank__service_mia_10hz(void) {

  const uint32_t mia_increment_value = 100;

  motor_cmd = motor_msg_bank__get_motor_cmd();
  if (dbc_service_mia_MOTOR_CMD(&motor_cmd, mia_increment_value)) {
    // Turn off LED to indicate sonar sensor MIA
    gpio__set(board_io__get_led0()); // in future use LCD
  }
}*/