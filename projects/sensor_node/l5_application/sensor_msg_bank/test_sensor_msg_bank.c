#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockbridge.h"
#include "Mockcan_bus.h"
// Include the source we wish to test
#include "sensor_msg_bank.h"

void setUp(void) {}

void tearDown(void) {}

void test_sensor_msg_bank__handle_msg(void) {
  can__rx_ExpectAnyArgsAndReturn(0);
  sensor_msg_bank__handle_msg();
}
