#pragma once

#include <stdbool.h>

typedef struct {
  float left;
  float middle;
  float right;
} ultra_sonic_sensor_data_s;

bool ultrasonic_sensor__init(void);

ultra_sonic_sensor_data_s ultrasonic_sensor__get_data(void);

void ultrasonic_sensor__run_once(void);