#include <stdio.h>

#include "adc.h"
#include "board_io.h"
#include "delay.h"
#include "gpio.h"
//#include "lpc40xx.h"
#include "sonar_adc_config.h"
#include "ultrasonic_sensor.h"

/* Voltage resolution = 0.0008057
   6.4mV per inch @3.3V VCC
   ~7.9 steps for one inch

   9.8 mV per inch @5V VCC
*/

static const float SCALE_FACTOR = 0.12589;

static const gpio_s trigger_pin = {GPIO__PORT_0, 6};
static ultra_sonic_sensor_data_s sensor_data;

static void ultrasonic_range_once(void) {
  gpio__set(trigger_pin);
  delay__us(50);
  gpio__reset(trigger_pin);

  delay__us(39000 * 3);
}

bool ultrasonic_sensor__init(void) {

  // adc 2, p0.25 - SENSOR SONAR LEFT
  // adc 4, p1.30 - SENSOR SONAR MIDDLE
  // adc 5, p1.31 - SENSOR SONAR RIGHT
  adc__initialize();
  gpio__construct_as_output(GPIO__PORT_0, 6);
  gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCTION_3); // ADC 4
  gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_3); // ADC 5
  gpio__construct_with_function(GPIO__PORT_0, 25, GPIO__FUNCTION_1); // ADC 2

  configure_sonar_sensors_adc();

  return true;
}

ultra_sonic_sensor_data_s ultrasonic_sensor__get_data() { return sensor_data; }

void ultrasonic_sensor__run_once() {
  // left, middle, right respectively
  for (int i = 0; i < 8; i++) {
    uint16_t adc_raw[3];

    // start range and wait till all sensors are done
    ultrasonic_range_once();

    adc_raw[0] = adc__get_adc_value(ADC__CHANNEL_2);
    adc_raw[1] = adc__get_adc_value(ADC__CHANNEL_4);
    adc_raw[2] = adc__get_adc_value(ADC__CHANNEL_5);

    // printf("adc2: %d\n", adc_raw[0]);
    sensor_data.left = ((float)adc_raw[0] * SCALE_FACTOR);   // / 2.0;
    sensor_data.middle = ((float)adc_raw[1] * SCALE_FACTOR); // / 2.0;
    sensor_data.right = ((float)adc_raw[2] * SCALE_FACTOR);  // / 2.0;

    if (sensor_data.left < 20) {
      gpio__reset(board_io__get_led3());
    } else {
      gpio__set(board_io__get_led3());
    }

    if (sensor_data.middle < 35) {
      gpio__reset(board_io__get_led2());
    } else {
      gpio__set(board_io__get_led2());
    }

    if (sensor_data.right < 20) {
      gpio__reset(board_io__get_led1());
    } else {
      gpio__set(board_io__get_led1());
    }
  }
}