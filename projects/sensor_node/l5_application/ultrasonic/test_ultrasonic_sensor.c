#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockadc.h"
#include "Mockboard_io.h"
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mocklpc40xx.h"
#include "Mocksonar_adc_config.h"
// Include the source we wish to test
#include "ultrasonic_sensor.h"
void setUp(void) {}

void test_ultrasonic_sensor__init(void) {
  gpio_s p1;
  adc__initialize_Expect();
  gpio__construct_as_output_ExpectAnyArgsAndReturn(p1);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(p1);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(p1);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(p1);
  configure_sonar_sensors_adc_Expect();
  TEST_ASSERT_TRUE(ultrasonic_sensor__init());
}

void test_ultrasonic_sensor__get_data() {
  ultra_sonic_sensor_data_s data;
  data = ultrasonic_sensor__get_data();
}

void test_ultrasonic_sensor__run_once(void) {
  for (int i = 0; i < 8; i++) {
    uint16_t val = 0;
    gpio_s p1, p2, p3;

    gpio__set_ExpectAnyArgs();
    delay__us_ExpectAnyArgs();
    gpio__reset_ExpectAnyArgs();
    delay__us_ExpectAnyArgs();

    adc__get_adc_value_ExpectAnyArgsAndReturn(val);
    adc__get_adc_value_ExpectAnyArgsAndReturn(val);
    adc__get_adc_value_ExpectAnyArgsAndReturn(val);

    board_io__get_led3_ExpectAndReturn(p1);
    gpio__reset_ExpectAnyArgs();

    board_io__get_led2_ExpectAndReturn(p2);
    gpio__reset_ExpectAnyArgs();

    board_io__get_led1_ExpectAndReturn(p3);
    gpio__reset_ExpectAnyArgs();
  }
  ultrasonic_sensor__run_once();
}
