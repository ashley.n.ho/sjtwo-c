#include "sonar_adc_config.h"
#include "lpc40xx.h"

void configure_sonar_sensors_adc(void) {
  LPC_IOCON->P1_30 &= ~(0b11 << 3);
  LPC_IOCON->P1_30 &= ~(1 << 7);

  LPC_IOCON->P1_31 &= ~(0b11 << 3);
  LPC_IOCON->P1_31 &= ~(1 << 7);

  LPC_IOCON->P0_25 &= ~(0b11 << 3);
  LPC_IOCON->P0_25 &= ~(1 << 7);
}