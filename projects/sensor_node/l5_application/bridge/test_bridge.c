#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"
#include "Mockclock.h"
#include "Mockemergency_stop.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"
#include "Mockuart_printf.h"
// Include the source we wish to test
#include "bridge.h"
#include "project.h"
void setUp(void) {}

void test_bridge__init(void) {
  gpio_s ret;
  clock__get_peripheral_clock_hz_ExpectAndReturn(96000000);
  uart__init_Expect(UART__2, 0, 9600);
  uart__init_IgnoreArg_peripheral_clock();

  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);

  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);

  bridge_init();
}

void test_bridge_get_data() {
  uart__get_ExpectAnyArgsAndReturn(0);
  bridge_get_data();
}

void test_bridge_send_data(void) {
  uart_printf_ExpectAnyArgsAndReturn(0);
  char data[10];
  bridge_send_data(data, sizeof(data), 0);
}

void test_bridge_get_data_emergency_stop() {
  char byte = 'S';
  uart__get_ExpectAndReturn(UART__2, NULL, 0, 1);
  uart__get_IgnoreArg_input_byte();
  uart__get_ReturnThruPtr_input_byte(&byte);

  set_emergency_stop_flag_Expect();

  bridge_get_data();
}
