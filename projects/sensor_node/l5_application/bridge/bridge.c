#include "bridge.h"
#include "can_bus.h"
#include "clock.h"
#include "emergency_stop.h"
#include "gpio.h"
#include "project.h"
#include "uart.h"
#include "uart_printf.h"
#include <stdio.h>
#include <stdlib.h>

#define UART_RX_BUF_SIZE 100
#define EMERGENCY_STOP_BYTE 'S'

static uart_e bridge_uart = UART__2;

static char uart_rx_buf[UART_RX_BUF_SIZE];
unsigned int buf_idx = 0;

float dest_latitude;
float dest_longitude;

void bridge_init(void) {
  uart__init(bridge_uart, clock__get_peripheral_clock_hz(), 9600);

  gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1);

  QueueHandle_t rxq_handle = xQueueCreate(200, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(200, sizeof(char));
  uart__enable_queues(bridge_uart, rxq_handle, txq_handle);
}

// Expected format:
//"37000000,-12203218\n"
// converts to:
//"37.000000,-122.03218\n"
static void process_uart_data_buf(void) {
  int temp;
  char *token;
  int coord_received = 0;

  token = strtok(uart_rx_buf, ",");
  temp = atoi(token);
  if (temp != 0) {
    dest_latitude = temp / 1000000.0f;
    printf("la: %f ", (double)dest_latitude);
    coord_received++;
  }

  token = strtok(NULL, ",");
  temp = atoi(token);
  if (temp != 0) {
    dest_longitude = temp / 1000000.0f;
    printf("lo: %f\n", (double)dest_longitude);
    coord_received++;
  }
  if (coord_received == 2) {
    dbc_GPS_DESTINATION_LOCATION_s dest_location;
    dest_location.GPS_DEST_LATITUDE_SCALED_100000 = dest_latitude * 100000;
    dest_location.GPS_DEST_LONGITUDE_SCALED_100000 = dest_longitude * 100000;
    can__msg_t can_msg = {};
    dbc_message_header_t header;
    header = dbc_encode_GPS_DESTINATION_LOCATION(can_msg.data.bytes, &dest_location);

    can_msg.msg_id = header.message_id;
    can_msg.frame_fields.data_len = header.message_dlc;
    can__tx(can1, &can_msg, 0);
  }
}

void bridge_get_data(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(bridge_uart, &byte, zero_timeout)) {
    printf("BLE RX: %c,(%d)\n", byte, (int)byte);

    if (byte == EMERGENCY_STOP_BYTE) {
      set_emergency_stop_flag();
      return;
    }

    uart_rx_buf[buf_idx++] = byte;

    if (byte == '\n') {
      process_uart_data_buf();
      buf_idx = 0;
    }
  }
}

void bridge_send_data(void *buf, uint8_t buf_size, uint8_t msg_id) {
  // Format the float value from the buffer
  char tx_buf[30];
  if (msg_id == -1) {
    float speed = *(float *)buf;
    snprintf(tx_buf, sizeof(tx_buf), "spd: %f\n", (double)speed);
  } else if (msg_id == 1) {
    dbc_GEO_STATUS_s *msg = (dbc_GEO_STATUS_s *)buf;
    snprintf(tx_buf, sizeof(tx_buf), "H: %d\nB: %d\n", msg->GEO_STATUS_COMPASS_HEADING,
             msg->GEO_STATUS_COMPASS_BEARING);
  }
  // while (gpio__get(HW_CTS_PIN) == 0) {};
  uart_printf(bridge_uart, "%s", tx_buf);
  // printf("sent: %s\n", tx_buf);
}