#pragma once

#include <stdint.h>

void bridge_init(void);

void bridge_send_data(void *buf, uint8_t buf_size, uint8_t msg_id);
void bridge_get_data(void);
