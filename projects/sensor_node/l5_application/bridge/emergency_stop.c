#include "emergency_stop.h"
#include "can_bus.h"
#include "project.h"

#include <stdbool.h>

/* An emergency stop will require the sensor node to be manually reset*/

static bool stop_vehicle = false;

void set_emergency_stop_flag(void) { stop_vehicle = true; }

void emergency_stop_run_once(void) {
  dbc_MOTOR_STOP_s stop_msg;
  stop_msg.MOTOR_STOP_stop = stop_vehicle;
  can__msg_t can_msg = {};
  dbc_message_header_t header;
  header = dbc_encode_MOTOR_STOP(can_msg.data.bytes, &stop_msg);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}