#include <stdio.h>

#include "can_bus.h"
#include "math.h"
#include "project.h"
#include "sensor_sonar_transmitter.h"
#include "ultrasonic_sensor.h"

static void fill_sensor_sonar_struct(dbc_SENSOR_SONARS_s *sensor_struct) {
  ultra_sonic_sensor_data_s data = ultrasonic_sensor__get_data();
  sensor_struct->SENSOR_SONARS_left = roundf(data.left);
  sensor_struct->SENSOR_SONARS_middle = roundf(data.middle);
  sensor_struct->SENSOR_SONARS_right = roundf(data.right);
}

static bool can_tx_send_sensor_sonar_data(void) {
  bool success = false;
  static dbc_SENSOR_SONARS_s sensor_struct;
  can__msg_t can_msg = {};
  dbc_message_header_t header;

  fill_sensor_sonar_struct(&sensor_struct);
  header = dbc_encode_SENSOR_SONARS(can_msg.data.bytes, &sensor_struct);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  success = can__tx(can1, &can_msg, 0);

  return success;
}

bool sensors__transmit_once() { return can_tx_send_sensor_sonar_data(); }