#include "periodic_callbacks.h"

#include "board_io.h"
#include "bridge.h"
#include "can_bus.h"
#include "can_bus_initializer.h"
#include "delay.h"
#include "emergency_stop.h"
#include "gpio.h"
#include "heading_sensor.h"
#include "sensor_msg_bank.h"
#include "sensor_sonar_transmitter.h"
#include "ultrasonic_sensor.h"
/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */

void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_bus_initializer_run_once(can1, 100, 200, 200);
  ultrasonic_sensor__init();
  heading_sensor_init();
  bridge_init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  if (callback_count == 0) {
    delay__us(450000); // start up delay
    return;
  }
  ultra_sonic_sensor_data_s data;
  ultrasonic_sensor__run_once();
  data = ultrasonic_sensor__get_data();
  unsigned int heading = heading_sensor_get_heading();
  printf("L: %f - ", (double)data.left);
  printf("M: %f - ", (double)data.middle);
  printf("R: %f\n", (double)data.right);
  printf("HD: %d\n", heading);
  // Add your code here
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  sensors__transmit_once();
  bridge_get_data();
  sensor_msg_bank__handle_msg();
  heading_sensor_run_once();
}
void periodic_callbacks__100Hz(uint32_t callback_count) { emergency_stop_run_once(); }

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}