#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"
#include "Mockultrasonic_sensor.h"
// Include the source we wish to test
#include "sensor_sonar_transmitter.h"

void setUp(void) {}

void test_sensors_transmit_once(void) {
  ultra_sonic_sensor_data_s sensor_data;
  ultrasonic_sensor__get_data_ExpectAndReturn(sensor_data);
  can__tx_ExpectAnyArgsAndReturn(1);
  TEST_ASSERT_TRUE(sensors__transmit_once());
}