#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockbridge.h"
#include "Mockcan_bus_initializer.h"
#include "Mockdelay.h"
#include "Mockemergency_stop.h"
#include "Mockgpio.h"
#include "Mockheading_sensor.h"
#include "Mocksensor_msg_bank.h"
#include "Mocksensor_sonar_transmitter.h"
#include "Mockultrasonic_sensor.h"
// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer_run_once_ExpectAnyArgsAndReturn(0);
  ultrasonic_sensor__init_ExpectAndReturn(1);
  heading_sensor_init_Expect();
  bridge_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  ultra_sonic_sensor_data_s data;
  for (int i = 0; i < 10; i++) {
    if (i == 0) {
      delay__us_ExpectAnyArgs();
    } else {
      ultrasonic_sensor__run_once_Expect();
      ultrasonic_sensor__get_data_ExpectAndReturn(data);
      heading_sensor_get_heading_ExpectAndReturn(10);
    }
    periodic_callbacks__1Hz(i);
  }
}

void test__periodic_callbacks__10Hz(void) {
  bridge_get_data_Expect();
  sensors__transmit_once_ExpectAndReturn(1);
  sensor_msg_bank__handle_msg_Expect();
  heading_sensor_run_once_Expect();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  emergency_stop_run_once_Expect();
  periodic_callbacks__100Hz(0);
}