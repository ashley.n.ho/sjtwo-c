#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"
// Include the source we wish to test
#include "heading_sensor.c"

void setUp(void) {}

void tearDown(void) {}

void test_heading_sensor_init(void) {
  gpio_s ret;
  clock__get_peripheral_clock_hz_ExpectAndReturn(96000000);
  uart__init_Expect(UART__3, 0, 9600);
  uart__init_IgnoreArg_peripheral_clock();

  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);

  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);
  heading_sensor_init();
}

void test_heading_sensor_run_once(void) {
  uart__get_ExpectAnyArgsAndReturn(0);
  heading_sensor_run_once();
}

void test_parse_data(void) {
  char data[] = {'3', '1', '0', '\n'};
  for (int i = 0; i < sizeof(data); i++) {
    uart_rx_buf[i] = data[i];
  }

  can__tx_ExpectAnyArgsAndReturn(0);

  parse_buffer();
  TEST_ASSERT_EQUAL(310, heading_angle);
}

void test_parse_data_310(void) {
  char data[] = {'0', '\n'};
  for (int i = 0; i < sizeof(data); i++) {
    uart_rx_buf[i] = data[i];
  }
  can__tx_ExpectAnyArgsAndReturn(0);
  parse_buffer();
  TEST_ASSERT_EQUAL(0, heading_angle);
}

void test_parse_data_270(void) {
  char data[] = {'2', '7', '0', '\n'};
  for (int i = 0; i < sizeof(data); i++) {
    uart_rx_buf[i] = data[i];
  }
  can__tx_ExpectAnyArgsAndReturn(0);
  parse_buffer();
  TEST_ASSERT_EQUAL(270, heading_angle);
}
