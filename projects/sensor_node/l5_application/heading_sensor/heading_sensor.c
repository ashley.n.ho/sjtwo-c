#include "heading_sensor.h"
#include "clock.h"
/*
 Inclometer driver
 Incometer is preconfigured to only output angle data
 Driver assumes this configuration
*/

#include "can_bus.h"
#include "gpio.h"
#include "project.h"
#include "uart.h"
#include <stdio.h>
#include <stdlib.h>

#define UART_RX_BUF_SIZE 50

#define PACKET_START_BYTE 0x55
#define ANGLE_PACKET_ID_BYTE 0x53
#define ANGLE_PACKET_SIZE_BYTES 11

static uart_e heading_sensor_uart = UART__3;

static char uart_rx_buf[ANGLE_PACKET_SIZE_BYTES];
unsigned int buf_write_idx = 0;
uint16_t heading_angle;

uint16_t heading_sensor_get_heading(void) { return heading_angle; }

void heading_sensor_init(void) {
  uart__init(heading_sensor_uart, clock__get_peripheral_clock_hz(), 9600);

  // UART3 is on P4.28, P4.29
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2);

  QueueHandle_t rxq_handle = xQueueCreate(4096, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(200, sizeof(char));

  uart__enable_queues(UART__3, rxq_handle, txq_handle);
}

static void parse_buffer(void) {
  char *buf = (char *)uart_rx_buf;
  heading_angle = atoi(buf);

  dbc_GPS_HEADING_s heading;
  heading.GPS_HEADING_RAW_DEGREES = heading_angle;
  can__msg_t can_msg = {};
  dbc_message_header_t header;
  header = dbc_encode_GPS_HEADING(can_msg.data.bytes, &heading);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

void heading_sensor_run_once(void) {
  char byte;

  while (uart__get(heading_sensor_uart, &byte, 0)) {
    // printf("received byte: %x\n", byte);

    uart_rx_buf[buf_write_idx++] = byte;
    if (byte == '\n') {
      parse_buffer();
      buf_write_idx = 0;
    }
  }
}
