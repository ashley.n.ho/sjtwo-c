#pragma once

#include <stdint.h>

void heading_sensor_init(void);
void heading_sensor_run_once(void);
uint16_t heading_sensor_get_heading(void);